--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
-- Loot Item 
-- Represents an undistributed item. 
--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
local LootItem = {}

----------------------------------------------------------------------------------
-- Initialization
----------------------------------------------------------------------------------
function LootItem:New(wndParent, tLootItem, bIsSelectable)
	local meta = {
		tGenTooltipTimer = nil,
		tCallback        = nil,
		tItem            = tLootItem.itemDrop,
		tLooters         = tLootItem.tLooters,
		tLootersOoR      = tLootItem.tLootersOutOfRange,
		nUUID            = tLootItem.nLootId,
		wndHandle        = nil,
		wndHighlight     = nil,
		wndBorder        = nil,
		wndIcon          = nil,
		wndInlineInfo    = nil,
		wndTooltip       = nil,
		wndTooltipComp   = nil,
		bIsGenTooltip    = false,
		bIsSelectable    = bIsSelectable,
		bIsSelected      = false,
		bIsRolledFor     = false,
		bIsLocked        = false,
		btn              = nil,
		fCompare         = function() return nil end,
	}

	local self = ClassFactory:SetupInstance(meta, LootItem)
	
	self.wndHandle    = XMLFactory:CreateWindow("LootItem", wndParent, self)
	self.wndHighlight = self.wndHandle:FindChild("Highlight")
	self.wndBorder    = self.wndHandle:FindChild("Border")
	self.wndIcon      = self.wndHandle:FindChild("Icon")   
	self.btn          = self.wndHandle:FindChild("Button")

	self.wndHighlight:Show(false, true)
	self.wndIcon:SetSprite(self:GetIcon())
	self.wndBorder:SetSprite(Utils:GetItemRarity(self)) 
	
	-- Events
    if self.bIsSelectable then
		self.btn:AddEventHandler("ButtonSignal", "OnButtonPressed", self)
	else 
		self.wndBorder:SetBGColor("ffffffff")
	end
	
	return self
end

function LootItem:Destroy()
	if self.wndHandle ~= nil then
		self.wndHandle:Destroy()
	end
	
	if self.wndTooltip and self.wndTooltip:IsValid() then
		self.wndTooltip:Destroy()
	end
	
	if self.wndTooltipCompand and self.wndTooltipComp:IsValid() then
		self.wndTooltipComp:Destroy()
	end
end

----------------------------------------------------------------------------------
-- Functions
----------------------------------------------------------------------------------
function LootItem:Update(tLootItem)
	self.tLooters    = tLootItem.tLooters
	self.tLootersOoR = tLootItem.tLootersOutOfRange
end

function LootItem:GenerateTooltip()
	-- No need to do anything if generating tooltip is ongoing.
	if self.bIsGenTooltip then
		return 
	end
	
	-- In case we have a valid tooltip nothing more can be done.
	if self.wndTooltip ~= nil and self.wndTooltip:IsValid() then
		return
	end

	-- Beware -- 
	-- Calling 'OnGenerateTooltip' from here will not work "to earily"!
	-- Postponing the generation to be post 'OnLoad' works. The timer is 
	-- unlikely to run more then once for each item.
	--
	-- Also note this workaround only affect tooltips for loot that exists 
	-- when the addon is loaded (for instance if you do /reloadui while having 
	-- undistributed loot).
	self.tGenTooltipTimer = ApolloTimer.Create(1, true, "OnGenerateTooltip", self)
end

function LootItem:SetCallback(tCallback)
	self.tCallback = tCallback
end

function LootItem:SetAnchor(tAnchor)
	local tP = tAnchor.tPoints
	local tO = tAnchor.tOffsets

	self.wndHandle:SetAnchorPoints(tP[1], tP[2], tP[3], tP[4])
	self.wndHandle:SetAnchorOffsets(tO[1], tO[2], tO[3], tO[4])
end

function LootItem:SetIsRolledFor(bVal)
	self.bIsRolledFor = bVal
end

function LootItem:GetSize()
	return {
		nWidth  = self.wndHandle:GetWidth(), 
		nHeight = self.wndHandle:GetHeight()
	}
end

function LootItem:GetId()
	return self.nUUID
end

function LootItem:HaveId(nId)
	return self.nUUID == nId
end

function LootItem:GetName()
	return self.tItem:GetName()
end

function LootItem:GetIcon()
	return self.tItem:GetIcon()
end

function LootItem:GetQuality()
	return self.tItem:GetItemQuality()
end

function LootItem:GetPower()
	return self.tItem:GetItemPower()
end

function LootItem:GetItemLevel()
	return self.tItem:GetEffectiveLevel()
end

function LootItem:GetEligibleRecipients()
	local tEligibleRecipients = {}
	
	if self.tLooters ~= nil then
		for _, tUnit in pairs(self.tLooters) do
			table.insert(tEligibleRecipients, {
				strUnitName = tUnit:GetName(),
				tUnit       = tUnit
			})
		end
	end
		
	if self.tLootersOoR ~= nil then
		for _, strOORLooter in pairs(self.tLootersOoR) do
			table.insert(tEligibleRecipients, {
				strUnitName = strOORLooter,
				tUnit       = nil
			})
		end
	end
	return tEligibleRecipients 
end

function LootItem:GetItem()
	return self.tItem
end

function LootItem:IsRolledFor()
	return self.bIsRolledFor
end

function LootItem:Unselect(bShouldPreventEvent)
	if self.bIsSelectable and not self.bIsLocked then 
		self.wndHighlight:Show(false, false)
		self.bIsSelected = false
		
		if not bShouldPreventEvent then
			Utils:FireEvent(self.tCallback, "OnItemUnselected", self)
		end
	end
end

function LootItem:Select(bShouldPreventEvent)
	if self.bIsSelectable and not self.bIsLocked then 
		self.wndHighlight:Show(true, false)
		self.bIsSelected = true
		
		if not bShouldPreventEvent then
			Utils:PlaySound(86)
			Utils:FireEvent(self.tCallback, "OnItemSelected", self)
		end
	elseif self.bIsLocked then
		Utils:PlaySound(220)
	end	
end

function LootItem:GetRandomRecipient(nTries)
	if nTries == nil then
		nTries = 0
	end
	
	local nLooters = #self.tLooters
	local nIndex   = Utils:Random(nLooters)
	
	local tLooterUnit = self.tLooters[nIndex]
	
	if tLooterUnit:IsValid() then
		return tLooterUnit 
	end
	
	if nTries > 5 then
		return nil
	end
	
	return self:GetRandomRecipient(nTries + 1)
end

function LootItem:IsEligible(strUnitName)
	for nIndex, tLooterUnit in ipairs(self.tLooters) do
		if tLooterUnit:GetName() == strUnitName then
			return true
		end
	end
		
	return false
end

function LootItem:IsSelected() 
	return self.bIsSelected
end

function LootItem:SetLock(bShouldLock)
	self.bIsLocked = bShouldLock
end

function LootItem:SetCompareFunction(fCompare)
	self.fCompare = fCompare
end

----------------------------------------------------------------------------------
-- Event handlers
----------------------------------------------------------------------------------
function LootItem:HandleOnGenerateTooltip()
	self.wndTooltip, self.wndTooltipComp = Tooltip.GetItemTooltipForm(self, self.btn, self.tItem, {
			bPrimary    = true, 
			bSelling    = false, 
			itemCompare = self.fCompare(self.tItem)
	})
	
	Utils:Log("HandleOnGenerateTooltip")
	Utils:Log(self.wndTooltip)
	if self.wndTooltip ~= nil and self.wndTooltip:IsValid() then
		self.tGenTooltipTimer:Stop()
		self.tGenTooltipTimer = nil
	end
end
----------------------------------------------------------------------------------
-- Events
----------------------------------------------------------------------------------
function LootItem:OnGenerateTooltip(...)
	Utils:InvokeEventHandler(self, "LootItem", "OnGenerateTooltip", ...)
end

function LootItem:OnButtonPressed(wndHandler, wndControl, eMouseButton)
	if self.bIsSelected then
		self:Unselect()
	else 
		self:Select()
	end
end

function LootItem:OnMouseUp(wndHandler, wndControl, eMouseButton)
	Event_FireGenericEvent("GenericEvent_ContextMenuItem", self.tItem)
end

ClassFactory:RegisterClass("LootItem", LootItem)
