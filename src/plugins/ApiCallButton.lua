------------------------------------------------------------------------------------------------
-- ApiCallButton  
-- The MIT License (MIT)
--
-- Copyright (c) 2015
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE. 
--
-- 
-- ApiCallButton
-- This class represent a button in the UI. It encapsulate the an API call 
-- to an external service. 
-- 
-- This uses the shared clipboard resource available in Windows. In order for
-- external data to be fetch a local client MUST be running on the computer.

-- 
-- Author: Johan Lindstr�m (Jabbit-EU, Joxye Nadrax / Wildstar)
-------------------------------------------------------------------------------------------------
local ApiCallButton = {}
local RaidLooter    = Apollo.GetAddon("RaidLooter")
local JSON          = Apollo.GetPackage("Lib:dkJSON-2.5").tPackage
local ClassFactory  = RaidLooter:GetSingleton("ClassFactory")
local XMLFactory    = RaidLooter:GetSingleton("XMLFactory")
local Utils         = RaidLooter:GetSingleton("Utils")

-----------------------------------------------------------------------------------------------
-- Initialization
-----------------------------------------------------------------------------------------------
function ApiCallButton:New(wndParent, strName)
	local meta = {
		bIsWaiting   = false,
		wndClipboard = RaidLooter:GetClipboard(),
		wndParent    = wndParent,
		wndHandle    = nil,
		btnHandle    = nil,
		strUrl       = nil,
		strQuery     = nil,
		tBody        = nil,
		tRequest     = nil,
		eMethod      = nil
	}
	
	local self = ClassFactory:SetupInstance(meta, ApiCallButton)
	self.wndHandle     = XMLFactory:CreateWindow("ActionConfirmButton", wndParent, self)
	self.tElapsedTimer = ApolloTimer.Create(0.250, true, "OnPollForResult", self)
	self.tElapsedTimer:Stop()
	self.btnHandle = self.wndHandle:FindChild("CTCButton")
	self.btnHandle:SetText(strName)
	
	return self
end

-----------------------------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------------------------
function ApiCallButton:PrepareApiCall(eMethod, strUrl, strQuery, tBody)
	if strQuery ~= nil then
		strUrl = strUrl.."?"..strQuery
	end

	if eMethod == Utils.HttpMethod.GET then
		tBody = nil
	end
	
	local eCTC = GameLib.CodeEnumConfirmButtonType.CopyToClipboard
	local tRequest = {
  		method = eMethod,
  		url = strUrl,
  		headers = {
    		{ key = "Content-Type", Value = "application/json" }
  		},
		body = tBody
	} 
	
	self.btnHandle:SetActionData(eCTC, JSON.encode(tRequest));
end

-----------------------------------------------------------------------------------------------
-- Event handlers
-----------------------------------------------------------------------------------------------
function ApiCallButton:HandleOnApiCall(wndHandler, wndControl)
	self.btnHandle:Show(false)
	self.tElapsedTimer:Start()
end

function ApiCallButton:OnPollForResult()
	self.wndClipboard:SetText("")
	self.wndClipboard:PasteTextFromClipboard()
	local data = self.wndClipboard:GetText()
	
	if string.sub(data,1,1) == "{" then
		local response = JSON.decode(data)
	
		if response ~= nil then
			self.wndClipboard:SetText("")
			self.tElapsedTimer:Stop()
			
			for nIndex, strUnitName in pairs(response.body.Participants) do
				local tRoller = ClassFactory:CreateInstance("Roller", self.wndParent, false)
				tRoller:Set(nIndex, strUnitName , "", nIndex)
		
				local nTop    = (nIndex - 1) * (20 + 2)
				local nBottom = nTop + 20
		
				tRoller:SetAnchor({
					tPoints  = {0, 0, 1, 0},
	    			tOffsets = {2, nTop, -2, nBottom}
				})
			end
		end
	end
end

-----------------------------------------------------------------------------------------------
-- Events
-----------------------------------------------------------------------------------------------
function ApiCallButton:OnApiCall(wndHandler, wndControl)
	Utils:InvokeEventHandler(self, "ApiCallButton", "OnApiCall", wndHandler, wndControl)
end

-----------------------------------------------------------------------------------------------
-- Make sure to "register" this class
-----------------------------------------------------------------------------------------------
ClassFactory:RegisterClass("ApiCallButton", ApiCallButton)