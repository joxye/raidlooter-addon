------------------------------------------------------------------------------------------------
-- The MIT License (MIT)
--
-- Copyright (c) 2015
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
-- 
-- History View 
-- Represents that history view/tab of the addon.  
--
-- Author: Johan Lindstr�m (Jabbit-EU, Joxye Nadrax / Wildstar)
-------------------------------------------------------------------------------------------------
local HistoryView  = {}
local RaidLooter   = Apollo.GetAddon("RaidLooter")
local ClassFactory = RaidLooter:GetSingleton("ClassFactory")
local XMLFactory   = RaidLooter:GetSingleton("XMLFactory")
local Utils        = RaidLooter:GetSingleton("Utils")
local ChatParser   = RaidLooter:GetSingleton("ChatParser")
local JSON         = Apollo.GetPackage("Lib:dkJSON-2.5").tPackage

-----------------------------------------------------------------------------------------------
-- Initialization
-----------------------------------------------------------------------------------------------
function HistoryView:New(bIsAuthorized, bIsInRaid)
	local meta = {
		wndMain       = nil,
		wndFrame      = nil,
		btnTab        = nil,
		btnCopyTest   = nil,
		bIsAuthorized = bIsAuthorized,
		bIsInRaid     = bIsInRaid
	}
	return ClassFactory:SetupInstance(meta, HistoryView)
end

function HistoryView:Destroy()
	
end

function HistoryView:Load(bShouldShow)
	if self.wndFrame ~= nil then 
		self.wndFrame:Close()
	end

	self.wndMain          = RaidLooter:GetWindow()
	self.wndFrame         = self.wndMain:FindChild("HistoryPage")
	self.wndDataArea      = self.wndMain:FindChild("DataArea")
	self.btnTab           = self.wndMain:FindChild("HistoryPageButton")
	self.btnRaidSummery   = ClassFactory:CreateInstance("ApiCallButton", self.wndDataArea, "Update")

	self.btnRaidSummery:PrepareApiCall(
	    Utils.HttpMethod.GET,
		"internal://raid_summery",
		"limit=Raiders"
	)
	
	self.btnTab:AttachWindow(self.wndFrame)
	self.btnTab:SetCheck(bShouldShow)
end

-----------------------------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------
-- Event handlers
-----------------------------------------------------------------------------------------------
	
-----------------------------------------------------------------------------------------------
-- Events
-----------------------------------------------------------------------------------------------


function HistoryView:OnHistoryClear()	
	
	--self.wndClipboard:SetText("")
	--self.wndClipboard:PasteTextFromClipboard()
	--local data = self.wndClipboard:GetText()
	
	--local response = JSON.decode(data)
	
	--for nIndex, strUnitName in pairs(response.body.Participants) do
	--	local tRoller = ClassFactory:CreateInstance("Roller", self.wndDataArea, false)
	--	tRoller:Set(nIndex, strUnitName , "", nIndex)
	--	
	--	local nTop    = (nIndex - 1) * (20 + 2)
	--	local nBottom = nTop + 20
	--	
	--	tRoller:SetAnchor({
	--		tPoints  = {0, 0, 1, 0},
	--	    tOffsets = {2, nTop, -2, nBottom}
	--	})
	--end
	--Utils:Log(response)
end

-----------------------------------------------------------------------------------------------
-- Make sure to "register" this class
-----------------------------------------------------------------------------------------------
ClassFactory:RegisterClass("HistoryView", HistoryView)