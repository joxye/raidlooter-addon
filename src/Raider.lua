--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
-- Raider
-- Represents a member of the raid party 
--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
local Raider = {}

----------------------------------------------------------------------------------
-- Initialization
----------------------------------------------------------------------------------
function Raider:New(wndParent, tUnit, strUnitName)
	local meta = {
		wndParent      = wndParent,
		wndHandle      = nil,
		wndIcon        = nil,
		wndName        = nil,
		wndBG          = nil,
		wndAnim        = nil,
		wndInspectAnim = nil,
		wndWarn        = nil,
		wndInset       = nil,
		btn            = nil,
		tCallback      = nil,
		tUnit          = tUnit,
		strName        = "",
		strUnitName    = strUnitName,
		strClassSprite = "",
		bIsMouseInside = false, 
		bIsSelected    = false,
		bIsOutOfRange  = false,
		bIsInspecting  = false,
	}
	
    local self = ClassFactory:SetupInstance(meta, Raider)
	
	self.wndHandle      = XMLFactory:CreateWindow("Raider", wndParent, self)
	self.btn            = self.wndHandle:FindChild("Button")
	self.wndIcon        = self.wndHandle:FindChild("ClassIcon")
	self.wndName        = self.wndHandle:FindChild("Name")
	self.wndInset       = self.wndHandle:FindChild("Inset")
	self.wndBG          = self.wndHandle:FindChild("Background")
	self.wndAnim        = self.wndHandle:FindChild("SelectedAnimation")
	self.wndWarn        = self.wndHandle:FindChild("Indicator")
	self.wndInspectAnim = self.wndHandle:FindChild("InspectAnimation")

	self.wndInspectAnim:Show(true, true)
	self.wndInspectAnim:SetBGColor("0080ff00")
	self.wndAnim:Show(true, true)
	self.wndAnim:SetBGColor("00ffffff")
	self.wndWarn:Show(false, true)
	self.wndName:SetText(self.strName)
	
	-- Events
	self.btn:AddEventHandler("MouseEnter",   "OnMouseEnter",    self)
	self.btn:AddEventHandler("MouseExit",    "OnMouseExit",     self)
	self.btn:AddEventHandler("ButtonSignal", "OnButtonPressed", self)
	self.btn:AddEventHandler("MouseButtonUp", "OnMouseButtonUp", self)
	self.bIsOutOfRange = tUnit == nil or not tUnit:IsValid()
	self:UpdateOutOfRange(tUnit, strUnitName)
	
    return self
end	

function Raider:Destroy()
	if self.wndHandle ~= nil then
		self.wndHandle:Destroy()
	end
end

----------------------------------------------------------------------------------
-- Functions
----------------------------------------------------------------------------------
function Raider:UpdateOutOfRange(tUnit, strUnitName)
	local bIsOutOfRange = tUnit == nil or not tUnit:IsValid()
	
	if bIsOutOfRange then
		self.strName        = strUnitName
		self.strClassSprite = ""
	else 
		self.strName        = tUnit:GetName()
		self.strClassSprite = Utils:GetClassIcon(tUnit)
	end
	
	self.wndIcon:SetSprite(self.strClassSprite)
	
	if self.strName ~= nil then
		self.strName = self.strName:gmatch("[^ ]+")()
	end
	
	self:SetIsOutOfRange(bIsOutOfRange)
end

function Raider:SetIsOutOfRange(bIsOutOfRange)
	self.bIsOutOfRange = bIsOutOfRange
	self.wndWarn:Show(bIsOutOfRange)
	self.wndInset:Show(not bIsOutOfRange)
	
	if bIsOutOfRange then
		self.wndName:SetText(self.strName .. " (OoR)")
	else 
		self.wndName:SetText(self.strName)
	end
end

function Raider:SetAnchor(tAnchor)
	local tP = tAnchor.tPoints
	local tO = tAnchor.tOffsets

	self.wndHandle:SetAnchorPoints(tP[1], tP[2], tP[3], tP[4])
	self.wndHandle:SetAnchorOffsets(tO[1], tO[2], tO[3], tO[4])
end

function Raider:Unselect()
	if self.bIsSelected then
		self.bIsSelected = false
		self.btn:SetCheck(self.bIsSelected)
		
		-- Workaround :Show(false) triggers MouseExit/MouseEnter
		self.wndAnim:SetBGColor("00ffffff")
		
		if not self.bIsMouseInside then
			self.wndBG:SetBGColor("0fffffff")
		end
	
		Utils:FireEvent(self.tCallback, "OnRecipientUnselected", self)
	end
end

function Raider:Select()
	if not self.bIsSelected and not self.bIsOutOfRange then
		self.bIsSelected = true
		self.btn:SetCheck(self.bIsSelected)
		
		-- Workaround :Show(false) triggers MouseExit/MouseEnter
		self.wndAnim:SetBGColor("ffffffff")
		
		if not self.bIsMouseInside then
			self.wndBG:SetBGColor("2fffffff")
		end
		
		Utils:PlaySound(86)
		Utils:FireEvent(self.tCallback, "OnRecipientSelected", self)
	elseif self.bIsOutOfRange then
		Utils:PlaySound(220)
	end	
end

function Raider:Show()
	self.wndHandle:Enable(true)
	self.wndHandle:Show(true)
end

function Raider:Hide()
	self.wndHandle:Enable(false)
	self.wndHandle:Show(false)
end

function Raider:SetCallback(tCallback)
	self.tCallback = tCallback
end

function Raider:SetUnit(tUnit)
	self.tUnit = tUnit
	self:UpdateOutOfRange(tUnit)
end

function Raider:GetClassSprite()
	return self.strClassSprite
end	

function Raider:GetFullname()
	return self.strUnitName
end

function Raider:GetUnit()
	return self.tUnit
end

function Raider:IsNamed(strUnitName)
	return self.strUnitName == strUnitName
end

function Raider:IsSelected()
	return self.bIsSelected
end

function Raider:StartInspection()
	if not self.bIsInspecting then
		self.wndInspectAnim:SetBGColor("ff80ff00")
		self.bIsInspecting = true
		Utils:FireEvent(self.tCallback, "OnPlayerInspectionStarted", self.tUnit, self)
	end
end

function Raider:StopInspection()
	if self.bIsInspecting then
		self.wndInspectAnim:SetBGColor("0080ff00")
		self.bIsInspecting = false
		Utils:FireEvent(self.tCallback, "OnPlayerInspectionStoped")
	end
end

----------------------------------------------------------------------------------
-- Events
----------------------------------------------------------------------------------
function Raider:OnMouseButtonUp(wndHandler, wndControl, eMouseButton)
	--if eMouseButton == GameLib.CodeEnumInputMouse.Right then 
	--	if self.bIsInspecting then
	--		self:StopInspection()
	--	else
	--		self:StartInspection()
	--	end
	--end
end

function Raider:OnButtonPressed(wndHandler, wndControl, eMouseButton)
	if eMouseButton == GameLib.CodeEnumInputMouse.Left then
		if self.bIsSelected then
			self:Unselect() 
		else 
			self:Select()
		end
	end
end

function Raider:OnMouseEnter(wndHandler, wndControl, x, y)
	self.bIsMouseInside = true
	self.wndBG:SetBGColor("5fffffff")
end

function Raider:OnMouseExit(wndHandler, wndControl, x, y)
	local a = self.bIsSelected and 2 or 0
	self.bIsMouseInside = false
	self.wndBG:SetBGColor(a .. "fffffff")
end

ClassFactory:RegisterClass("Raider", Raider)