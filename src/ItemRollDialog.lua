--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
-- ItemRollDialog   
--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
local ItemRollDialog = {}
local RollTypeEnum = {
	MainSpec = 1,
	OffSpec  = 2,
	Costume  = 3 
}

----------------------------------------------------------------------------------
-- Initialization
----------------------------------------------------------------------------------
function ItemRollDialog:New(tItem)
	local meta = {
		wndHandle       = nil,
		wndItemInset    = nil,
		wndExItemBorder = nil,
		wndItemSparks   = nil,
		wndItemName     = nil,
		wndItemType     = nil,
		bIsValid        = true,
		tLootItem       = nil,
		eRollType       = RollTypeEnum.MainSpec,
		btnMainSpec     = nil,
		btnOffSpec      = nil,
		btnCostume      = nil,
	}
	
	local self = ClassFactory:SetupInstance(meta, ItemRollDialog)
	  
	self.wndHandle    = XMLFactory:CreateTopLevelWindow("ItemDialog", self)
	self.wndItemInset = self.wndHandle:FindChild("ItemInset")
	self.tLootItem    = ClassFactory:CreateInstance("LootItem", self.wndItemInset, tItem, false)
	self.bIsValid     = self.tLootItem:IsEligible(Utils:GetPlayerName())
	
	if self.bIsValid then
		self.wndExItemBorder = self.wndHandle:FindChild("ItemToRollFor")
		self.wndItemSparks   = self.wndHandle:FindChild("Sparks")
		self.wndItemName     =	self.wndHandle:FindChild("ItemName")	 
		self.wndItemType     = self.wndHandle:FindChild("ItemType")
		self.btnMainSpec     = self.wndHandle:FindChild("MainSpecButton")
		self.btnOffSpec      = self.wndHandle:FindChild("OffspecButton")
		self.btnCostume      = self.wndHandle:FindChild("CostumeButton")
		
		local strTextColor = Utils:GetItemQualityString(self.tLootItem)
		local strExSprite  = Utils:GetItemExtendedRarity(self.tLootItem)
		
		self.wndItemType:SetText(self.tLootItem:GetItem():GetItemTypeName())
		self.wndItemName:SetText(self.tLootItem:GetName())
		self.wndItemName:SetTextColor(strTextColor)
		self.wndExItemBorder:SetSprite(strExSprite)
		self.btnMainSpec:SetCheck(true)
		self.btnMainSpec:SetBGColor(strTextColor)
		self.btnOffSpec:SetBGColor(strTextColor) 
		self.btnCostume:SetBGColor(strTextColor)
		
		self.tLootItem:SetCompareFunction(function(tItem) 
			return tItem:GetEquippedItemForItemType() 
		end)
		self.tLootItem:GenerateTooltip()
		self.wndHandle:Show(true)
	else
		self:Destroy()	
	end
	
	Utils:PlayNewRollSound()
	return self
end

function ItemRollDialog:Destroy()
	if self.wndHandle ~= nil then
		self.wndHandle:Destroy()
	end
	
	if self.tLootItem ~= nil then
		self.tLootItem:Destroy()
	end
	
	self.wndHandle    = nil
	self.wndItemInset = nil
	self.tLootItem    = nil
	self.bIsValid     = false
end

----------------------------------------------------------------------------------
-- Functions
----------------------------------------------------------------------------------
function ItemRollDialog:IsValid()
	return self.bIsValid
end

function ItemRollDialog:SetAnchor(tAnchor)
	local tP = tAnchor.tPoints
	local tO = tAnchor.tOffsets

	self.wndHandle:SetAnchorPoints(tP[1], tP[2], tP[3], tP[4])
	self.wndHandle:SetAnchorOffsets(tO[1], tO[2], tO[3], tO[4])
end

function ItemRollDialog:GetSize()
	return {
		nWidth = self.wndHandle:GetWidth(),
		nHeight = self.wndHandle:GetHeight()
	}
end

----------------------------------------------------------------------------------
-- Event handlers
----------------------------------------------------------------------------------
function ItemRollDialog:HandleOnRollTypeChanged(wndHandler, wndControl, eMouseButton)
	if wndControl:IsChecked() then
		local strButtonName = wndControl:GetName()
		 
		if strButtonName == "MainSpecButton" then
			self.eRollType = RollTypeEnum.MainSpec
		elseif strButtonName == "OffspecButton" then
			self.eRollType = RollTypeEnum.OffSpec
		elseif strButtonName == "CostumeButton" then
			self.eRollType = RollTypeEnum.Costume
		else
			self.eRollType = RollTypeEnum.MainSpec
		end
	end
end

function ItemRollDialog:HandleOnItemDialogClosed()
	self:Destroy()
end

function ItemRollDialog:HandleOnRollForItem()
	ChatSystemLib.Command("/roll")
	local nChNum     = Debug.bIsTest and ChatSystemLib.ChatChannel_Say or ChatSystemLib.ChatChannel_Party	
	local tChannel   = ChatSystemLib.GetChannels()[nChNum]
	
	if self.eRollType == RollTypeEnum.OffSpec then
		tChannel:Send("os")
	elseif self.eRollType == RollTypeEnum.Costume then
		tChannel:Send("costume")
	end
	
	self:Destroy()
	Utils:PlaySound(174)
end

function ItemRollDialog:HandleOnPassForItem()
	self:Destroy()
	Utils:PlaySound(200)
end

----------------------------------------------------------------------------------
-- Events
----------------------------------------------------------------------------------
function ItemRollDialog:OnRollTypeChanged(wndHandler, wndControl, eMouseButton)
	Utils:InvokeEventHandler(self, "ItemRollDialog", "OnRollTypeChanged", wndHandler, wndControl, eMouseButton)
end

function ItemRollDialog:OnItemDialogClosed()
	Utils:InvokeEventHandler(self, "ItemRollDialog", "OnItemDialogClosed")
end
function ItemRollDialog:OnRollForItem(wndHandler, wndControl, eMouseButton)
	Utils:InvokeEventHandler(self, "ItemRollDialog", "OnRollForItem")
end

function ItemRollDialog:OnPassForItem(wndHandler, wndControl, eMouseButton)
	Utils:InvokeEventHandler(self, "ItemRollDialog", "OnPassForItem")
end

ClassFactory:RegisterClass("ItemRollDialog", ItemRollDialog)
