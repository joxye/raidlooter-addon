--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
-- Raid Looter 
-- The MIT License (MIT)
--
-- Copyright (c) 2015 - 2016
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
-- 
--
-- OVERVIEW
-- This addon is intended to make loot distribution during raids simpler and 
-- faster. This addon can be used by anyone but the ONLY one that really needs 
-- it is the raider leader. This addon will show a compact overview of item's 
-- ready for distribution. The leader or raid assistant can initiate a roll for 
-- an item. The item will be linked to the party chat and all rolls made with 
-- /roll durring the roll-window will show in the addon window. 
--
-- The raid leader can assign items and/or randomize items. The leader may 
-- select multiple items at once for randomization. Eventhough raid assistants 
-- can access the management window, it is ONLY the raid leader that can 
-- distribute the loot.
--
-- Raiders will only see the LootWindow at which they can view the loot and see 
-- the latest rolls.
-- 
-- DEVELOPMENT
-- The source is available at Bitbucket
-- https://bitbucket.org/joxye/raidlooter-public/src
--
-- To load the project in Huston: File > Open > toc.xml
-- To package/install the addon run:
--   bin\build.exe --version Major.Minor.Rev
--
-- Adjust build.exe.lnk while developing a version is easiest.
-- The package will end up in the build directory. e.g. ./build/package-1.0.0
-- The build/install scripts will concatenate each source file specified in the 
-- toc.xml file to one master file. This is very useful as it allow us to change 
-- the toc.xml before distribution to 'select' which files that should be included
--
-- Addon errors
-- ClassFactory.bAllowLink = true -- Errors from other metatabales within raid-
-- looter will be cought and showed but memory problems will arise. Don't use 
-- in production.
--
-- Debugging
-- Change constants in Debug:New() especially set bIsDeveloping to ture.
-- then when in Wildstar run /rl-test 
--
-- Events
-- Incoming events passes throw the 'Utils:InvokeEventHandler' for debug purposes.
-- Sometimes events are forwarded directly by using 'Utils:FireEvent'
--
-- EXTRA NOTE
-- Since all Wildstar addons share the same global space it is important not to;
--     * relay on functions to exist,
--     * Redefine/change or remove standard functions.
-- 
-- AUTHOR 
-- Johan Lindstr�m (Jabbit-EU, Joxye Nadrax / Wildstar)
--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
require "Window"
require "GameLib"
require "Apollo"
require "Item"
require "GroupLib"
require "ChatSystemLib"

--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
-- Constants 
-- Contains constants such as item quality, class information etc.
--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
local MAJOR = 1
local MINOR = 0
local REV   = 0
local CURRENT_VERSION = MAJOR.."."..MINOR.."."..REV

local ItemInfo = {
	[Item.CodeEnumItemQuality.Inferior]  = {strName = "ItemQuality_Inferior",  strRarityBorder = "BK3:UI_RarityBorder_Grey",    strExtendedRarityBorder = "CRB_Tooltips:sprTooltip_Header_Silver"},
	[Item.CodeEnumItemQuality.Average]   = {strName = "ItemQuality_Average",   strRarityBorder = "BK3:UI_RarityBorder_White",   strExtendedRarityBorder = "CRB_Tooltips:sprTooltip_Header_White"},
	[Item.CodeEnumItemQuality.Good]      = {strName = "ItemQuality_Good",      strRarityBorder = "BK3:UI_RarityBorder_Green",   strExtendedRarityBorder = "CRB_Tooltips:sprTooltip_Header_Green"},
	[Item.CodeEnumItemQuality.Excellent] = {strName = "ItemQuality_Excellent", strRarityBorder = "BK3:UI_RarityBorder_Blue",    strExtendedRarityBorder = "CRB_Tooltips:sprTooltip_Header_Blue"},
	[Item.CodeEnumItemQuality.Superb]    = {strName = "ItemQuality_Superb",    strRarityBorder = "BK3:UI_RarityBorder_Purple",  strExtendedRarityBorder = "CRB_Tooltips:sprTooltip_Header_Purple"},
	[Item.CodeEnumItemQuality.Legendary] = {strName = "ItemQuality_Legendary", strRarityBorder = "BK3:UI_RarityBorder_Orange",  strExtendedRarityBorder = "CRB_Tooltips:sprTooltip_Header_Orange"},
	[Item.CodeEnumItemQuality.Artifact]  = {strName = "ItemQuality_Artifact",  strRarityBorder = "BK3:UI_RarityBorder_Magenta", strExtendedRarityBorder = "CRB_Tooltips:sprTooltip_Header_Pink"}
}
local ClassInfo = {
	[GameLib.CodeEnumClass.Esper] 		 = { strIcon = "Icon_Windows_UI_CRB_Esper",        strArmorType = "L" },
	[GameLib.CodeEnumClass.Medic] 		 = { strIcon = "Icon_Windows_UI_CRB_Medic",        strArmorType = "M" },
	[GameLib.CodeEnumClass.Stalker] 	 = { strIcon = "Icon_Windows_UI_CRB_Stalker",      strArmorType = "M" },
	[GameLib.CodeEnumClass.Warrior]      = { strIcon = "Icon_Windows_UI_CRB_Warrior",      strArmorType = "H" },
	[GameLib.CodeEnumClass.Engineer] 	 = { strIcon = "Icon_Windows_UI_CRB_Engineer",     strArmorType = "H" },
	[GameLib.CodeEnumClass.Spellslinger] = { strIcon = "Icon_Windows_UI_CRB_Spellslinger", strArmorType = "L" }
}

----------------------------------------------------------------------------------
-- Singleton classes.
-- Because loadstring does not compile with lexical scoping. I had to come up
-- with an workaround for singleton loading, so in order to add a singleton one 
-- must:
--
--   1. Declare a local variable here.
--   2. Call RaidLooter:RegisterSingleton(strName, tInstance)
--   3. And finally extend the if-else statement in RaidLooter:LoadSingleton
----------------------------------------------------------------------------------
local RaidLooter   = {} 
local Utils        = {}
local ClassFactory = {}
local XMLFactory   = {}
local ChatParser   = {}
local Settings     = {}
local Debug        = {}

----------------------------------------------------------------------------------
-- Initialization
----------------------------------------------------------------------------------
function RaidLooter:New()
	local meta = {
		tManagementWindow = nil,
		tLootWindow       = nil,
		tLoot             = nil,	
		tEventHandlers    = {},
		tSingletons       = {}
	}

    self = setmetatable(meta, RaidLooter)
	return self
end

function RaidLooter:Init()
	Apollo.RegisterAddon(self)
end

----------------------------------------------------------------------------------
-- Startup 
-- Called automatically at the very start of the addon cycle. Logging to the chat
-- window will not be available so /rover or a global variable must be used.
-- 
-- Beware that addon errors occurring in OnLoad, OnSave and/or OnRestore will not
-- be cought. 
----------------------------------------------------------------------------------
function RaidLooter:OnLoad()
	-- Register only the absolute minimum here. We should only load and bind our
	-- events when the player actually is in a raid group with master loot. 
	Apollo.RegisterSlashCommand("rl",                                  "OnRaidLooterCmd", self)
	Apollo.RegisterEventHandler("Group_Join",                          "OnJoinedGroup",  self)
	Apollo.RegisterEventHandler("InterfaceMenuListHasLoaded",          "OnInterfaceMenuListHasLoaded", self)
	Apollo.RegisterEventHandler("RaidLooterInterfaceList",             "OnRaidLooterCmd", self)
	Apollo.RegisterEventHandler("GenericEvent_ToggleGroupBag",         "OnToggleGroupBag", self)
	Apollo.RegisterEventHandler("GenericEvent_Raid_UncheckMasterLoot", "OnMasterLootUnchecked", self)
	
	-- Register "other" events as dynamic i.e place them in a table so we can 
	-- bind and unbind them as needed.
	self:RegisterDynamicEventHandler("Group_FlagsChanged",       "OnGroupFlagsChanged")
	self:RegisterDynamicEventHandler("MasterLootUpdate",         "OnMasterLootUpdate")
	self:RegisterDynamicEventHandler("LootAssigned",             "OnLootAssigned")
	self:RegisterDynamicEventHandler("Group_Leave",              "OnLeftGroup")
	self:RegisterDynamicEventHandler("Group_Disband",            "OnDisbandedGroup")
	self:RegisterDynamicEventHandler("Group_MemberFlagsChanged", "OnMemberGroupFlagsChanged")
	self:RegisterDynamicEventHandler("Inspect",                  "OnInspect")

	Debug:OnLoad()
	self:ActivateAddon()
end

function RaidLooter:OnSave(eLevel)
	Utils:FireEvent(self.tLootWindow, "OnSave", eLevel)
	Utils:FireEvent(self.tManagementWindow, "OnSave", eLevel)
	
	return Settings:Save(eLevel)
end

function RaidLooter:OnRestore(eLevel, tData)
	Settings:Load(eLevel, tData)
end

----------------------------------------------------------------------------------
-- Functions 
----------------------------------------------------------------------------------
function RaidLooter:ActivateAddon()
	if self:ShouldBeActive() and not self:IsActive() then
		XMLFactory:LoadAll("OnActivateAddon", self)
	end
end

function RaidLooter:DeactivateAddon()
	if not self:ShouldBeActive() and self:IsActive() then
		self:OnDeactivateAddon()
	end
end

function RaidLooter:IsActive()
	return self.tLootWindow ~= nil
end

function RaidLooter:ShouldBeActive()
	return Debug.bIsTest or Utils:IsMasterLootingOn()
end

function RaidLooter:AssignMasterLoot(tLootItem, tUnit)
	local nItemId = tLootItem:GetId()
	
	if tUnit ~= nil and tUnit:IsValid() and nItemId ~= nil then
		if Debug.bIsTest then
			for nIndex, tItem in pairs(Debug.tMasterLoot) do
				if tItem.nLootId == nItemId then
					table.remove(Debug.tMasterLoot, nIndex)
					
					Utils:FireEvent(
						self.tManagementWindow, "OnLootAssigned", 
						tLootItem:GetItem(), tUnit:GetName()
					)
					self:UpdateLoot()
					return
				end
			end
			
			Utils:Log("Unable to find item to assign")
		else
			GameLib.AssignMasterLoot(nItemId, tUnit)
		end
	end
end

function RaidLooter:GetManagementWindow()
	if self.tManagementWindow ~= nil then
		return self.tManagementWindow
	end
	return nil
end

function RaidLooter:GetLootWindow()
	return self.tLootWindow:GetWindow()
end

function RaidLooter:ToggleWindows()
	if self.tManagementWindow ~= nil then
		if self.tManagementWindow:IsShown() then
			self.tManagementWindow:Close()
		end
	end
	
	if self.tLootWindow:IsShown() then
		self.tLootWindow:Close()
	else
		self.tLootWindow:Open()
		self:UpdateLoot()
	end 
end

function RaidLooter:UpdateLoot()
	self.tLoot = Debug.bIsTest and Debug.tMasterLoot or GameLib.GetMasterLoot()
	Utils:FireEvent(self.tManagementWindow, "OnLootUpdate", self.tLoot)
	Utils:FireEvent(self.tLootWindow, "OnLootUpdate", self.tLoot)
end

function RaidLooter:RegisterDynamicEventHandler(strEventName, strFunctionName)
	table.insert(self.tEventHandlers, { 
		strEventName = strEventName, 
		strFunctionName = strFunctionName
	})
end

function RaidLooter:BindDynamicEventHandlers()
	for _, tEvent in pairs(self.tEventHandlers) do
		Apollo.RegisterEventHandler(tEvent.strEventName, tEvent.strFunctionName, self)
	end
end

function RaidLooter:UnbindDynamicEventHandlers()
	for _, tEvent in pairs(self.tEventHandlers) do
		Apollo.RemoveEventHandler(tEvent.strEventName, self)
	end
end

function RaidLooter:LoadSingleton(strName, tClassPrototype, ...)
	if self.tSingletons[strName] == nil then
		self.tSingletons[strName] = {
			tInstance = nil, 
			bIsLoaded = false
		}
	end

	local tSingleton = self.tSingletons[strName] 
	
	assert(tSingleton ~= nil, "This singleton " .. strName .. " is unknown!")
	assert(not tSingleton.bIsLoaded, "This singleton " .. strName .. " is already loaded!")
	
	tClassPrototype.__index = tClassPrototype
	fNew = tClassPrototype["New"]
	tSingleton.bIsLoaded = true
	
	-- If the prototype responds to "New" we call it to setup the instance. 
	if fNew ~= nil then
		tSingleton.tInstance = fNew(tClassPrototype, ...)
	else 
		tSingleton.tInstance = tClassPrototype
	end	
	
	assert(tSingleton.tInstance ~= nil, "The instance should not be nil!")
	
	if ClassFactory ~= nil then
		ClassFactory:LinkInstance(tSingleton.tInstance)
	end
	
	return tSingleton.tInstance
end

----------------------------------------------------------------------------------
-- Event handlers
----------------------------------------------------------------------------------
function RaidLooter:HandleOnRaidLooterCmd(cmd, params)
	if self:IsActive() then 
		self:ToggleWindows()
	else
		Utils:Log("Currently in the deactive state need to load XMLs ...")
		XMLFactory:LoadAll("OnForceLoad", self)
	end
end

function RaidLooter:HandleOnMasterLootUpdate() 
	self:UpdateLoot()
end

function RaidLooter:HandleOnMakeRoll()
	ChatSystemLib.Command("/roll")
end

function RaidLooter:HandleOnInterfaceMenuListHasLoaded()
	local strEventName = "InterfaceMenuList_NewAddOn"
	local tInfo = {"RaidLooterInterfaceList", "", "BK3:sprHolo_Alert_COMMAttachment_DruseraIcon"}
    Event_FireGenericEvent(strEventName, "RaidLooter", tInfo)
end

function RaidLooter:HandleOnJoinedGroup()
	self:ActivateAddon()
end

function RaidLooter:HandleOnLeftGroup()
	self:DeactivateAddon()
end

function RaidLooter:HandleOnDisbandedGroup()
	self:DeactivateAddon()
end

function RaidLooter:HandleOnGroupFlagsChanged()
	if Utils:IsMasterLootingOn() then
		self:ActivateAddon()
	else 
		self:DeactivateAddon() 
	end
end

function RaidLooter:HandleOnMemberGroupFlagsChanged(nMemberIdx, bFromPromotion, tChangedFlags)
	if self:IsActive() then
		local tMember = GroupLib.GetGroupMember(nMemberIdx)
		local tUnit   = GroupLib.GetUnitForGroupMember(nMemberIdx)
		
		if tMember ~= nil and tUnit ~= nil then
			local bIsConcerningYou = tUnit:IsThePlayer()
			local bIsManager       =  bIsConcerningYou and (tMember.bIsLeader or tMember.bRaidAssistant)
		
			if bIsConcerningYou then
				Utils:Log("IsConcerningYou") 
				if bFromPromotion then
					Utils:Log("bFromPromotion") 
					Utils:Log(bFromPromotion) 
					Utils:Log(not tMember.bIsLeader) 
					self.tLootWindow:ShowManagementButton(not tMember.bIsLeader)
				end
			end
			
			if bIsManager then
				if self.tManagementWindow == nil then
					self.tManagementWindow = ClassFactory:CreateInstance("ManagementWindow")
					self:UpdateLoot()
				end
			end	
		end
	end
end

function RaidLooter:HandleOnMasterLootUnchecked()
	if Utils:IsMasterLootingOn() then
		if self:IsActive() then
			self:OnDeactivateAddon()
		end
		
		self:ActivateAddon()
	else 
		self:DeactivateAddon() 
	end
end

function RaidLooter:HandleOnToggleGroupBag()
	self:OnMasterLootUpdate()
	self:ToggleWindows()
end

function RaidLooter:HandleOnActivateAddon()
	self.tLootWindow       = ClassFactory:CreateInstance("LootWindow")
	self.tManagementWindow = nil

	if Utils:IsAssistant() or Utils:IsLeader() then
	 	self.tManagementWindow = ClassFactory:CreateInstance("ManagementWindow")
	end
	
	if self.tLootWindow ~= nil then
		self.tLootWindow:Hide()
	end
	
	if self.tManagementWindow ~= nil then
		self.tManagementWindow:Hide()
	end
	
	self:BindDynamicEventHandlers()	
	self:UpdateLoot()
	ChatParser:Start()
end

function RaidLooter:HandleOnDeactivateAddon()
	ChatParser:ForceStop()
	XMLFactory:UnloadAll()
	
	if self.tLootWindow ~= nil then
		self.tLootWindow:Destroy()
		self.tLootWindow = nil
	end
	
	if self.tManagementWindow ~= nil then
		self.tManagementWindow:Destroy()
		self.tManagementWindow = nil
	end
	
	self:UnbindDynamicEventHandlers()
end

function RaidLooter:HandleOnForceLoad()
	self:OnActivateAddon()
	self.tLootWindow:Show(true)
	
	if self:IsAllowedToManage() then
		self.tManagementWindow:Show(true)
	end
	
	self:UpdateLoot()
	Utils:PlaySound(190)
end

----------------------------------------------------------------------------------
-- Events 
----------------------------------------------------------------------------------
function RaidLooter:OnRaidLooterCmd(...)
	Utils:InvokeEventHandler(self, "RaidLooter", "OnRaidLooterCmd", ...)
end
function RaidLooter:OnMasterLootUpdate(...)
	Utils:InvokeEventHandler(self, "RaidLooter", "OnMasterLootUpdate", ...)
end
function RaidLooter:OnInterfaceMenuListHasLoaded(...) 
	Utils:InvokeEventHandler(self, "RaidLooter", "OnInterfaceMenuListHasLoaded", ...)
end
function RaidLooter:OnJoinedGroup(...)
	Utils:InvokeEventHandler(self, "RaidLooter", "OnJoinedGroup", ...)
end
function RaidLooter:OnLeftGroup(...)
	Utils:InvokeEventHandler(self, "RaidLooter", "OnLeftGroup", ...)
end
function RaidLooter:OnDisbandedGroup(...)
	Utils:InvokeEventHandler(self, "RaidLooter", "OnDisbandedGroup", ...)
end
function RaidLooter:OnGroupFlagsChanged(...)
	Utils:InvokeEventHandler(self, "RaidLooter", "OnGroupFlagsChanged", ...)
end
function RaidLooter:OnMemberGroupFlagsChanged(...)
	Utils:InvokeEventHandler(self, "RaidLooter", "OnMemberGroupFlagsChanged", ...)	
end
function RaidLooter:OnMasterLootUnchecked(...) 
	Utils:InvokeEventHandler(self, "RaidLooter", "OnMasterLootUnchecked", ...)
end
function RaidLooter:HandleOnToggleGroupBag(...)
	Utils:InvokeEventHandler(self, "RaidLooter", "OnToggleGroupBag", ...)
end
function RaidLooter:OnActivateAddon(...)
	Utils:InvokeEventHandler(self, "RaidLooter", "OnActivateAddon", ...)
end
function RaidLooter:OnDeactivateAddon(...)
	Utils:InvokeEventHandler(self, "RaidLooter", "OnDeactivateAddon", ...)
end
function RaidLooter:OnForceLoad(...)
	Utils:InvokeEventHandler(self, "RaidLooter", "OnForceLoad", ...)
end

RaidLooter.__index = RaidLooter
RaidLooter = RaidLooter:New()
RaidLooter:Init()
