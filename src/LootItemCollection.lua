--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
-- Loot Item Collection
-- Represents a collection of undistributed items. 
--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
local LootItemCollection = {}

----------------------------------------------------------------------------------
-- Initialization
----------------------------------------------------------------------------------
function LootItemCollection:New(shouldCompareToSelf, bIsSelectable, wndTarget) 
	local meta = {
		bShouldCompareToSelf = shouldCompareToSelf,
		bIsSelectable        = bIsSelectable,
		bShouldPreventEvents = false, 
		tSelectedItems       = {},
		tItems               = {},	
		tCallback            = nil,
		wndHandle            = wndTarget,
		fCompare             = function() return nil end,
	}
	
	return ClassFactory:SetupInstance(meta, LootItemCollection)
end

function LootItemCollection:Destroy()
	for _, tItem in pairs(self.tItems) do
		tItem:Destroy()
	end

	self.tSelectedItems = nil
	self.tItems         = nil
	self.tCallback      = nil
	self.wndHandle      = nil
end

----------------------------------------------------------------------------------
-- Functions
----------------------------------------------------------------------------------
function LootItemCollection:UpdateLoot(tLoot)
	-- Copy all current items.
	local tKillItems = {}
	for _, tItem in pairs(self.tItems) do
		tKillItems[tItem:GetId()] = tItem
	end

	-- Attempt to insert new items.
	for _, tLootItem in pairs(tLoot) do
		local tExistingItem = self:GetItemWithUUID(tLootItem.nLootId) 
	
		if tExistingItem ~= nil then 
			tExistingItem:Update(tLootItem)
			tKillItems[tLootItem.nLootId] = nil
		else 
			local tNewLootItem = ClassFactory:CreateInstance("LootItem", self.wndHandle, tLootItem, self.bIsSelectable)
			tNewLootItem:SetCompareFunction(self.fCompare)
			tNewLootItem:SetCallback(self)
			tNewLootItem:GenerateTooltip()
			self:AddItem(tNewLootItem)
		end
	end

	-- Remove remaining items.
	for nKey, tItem in pairs(tKillItems) do
		self:RemoveItem(tItem, true)
	end
	
	-- Arrange items according to there position in self.tItems.
	self:Arrange() 
	self.wndHandle:RecalculateContentExtents()
end

function LootItemCollection:AddItem(tItem)
	
    local nInsertIndex = #self.tItems + 1 -- New last index

    for nIndex, tExistingItem in ipairs(self.tItems) do
		local bSortCond = nil
		local bIsEqual  = tItem:GetQuality() == tExistingItem:GetQuality()
		
		if bIsEqual then
			bIsEqaul = tItem:GetItemLevel() == tExistingItem:GetItemLevel()
			
			if bIsEqual then
				bSortCond = tItem:GetPower() >= tExistingItem:GetPower()
			else 
				bSortCond = tItem:GetItemLevel() > tExistingItem:GetItemLevel()
			end
		else 
			bSortCond = tItem:GetQuality() > tExistingItem:GetQuality()
		end

		if bSortCond then
			nInsertIndex = nIndex
			break
		end  
    end
    
    table.insert(self.tItems, nInsertIndex, tItem)
end

function LootItemCollection:RemoveItem(tItem, bPreventArrange)
	-- This item must lose its selection.
	tItem:Unselect()

	for nIndex, tExistingItem in ipairs(self.tItems) do
		if tItem:GetId() == tExistingItem:GetId() then
			tItem:Destroy()
			table.remove(self.tItems, nIndex)
			
			if not bPreventArrange then
				self:Arrange()
			end
			
			break
		end
	end
end

function LootItemCollection:Arrange()
	if Utils:IsTableEmpty(self.tItems) then
		return
	else 
		-- (Pre)calculate some variables
		local nSW     = 6                                          -- Estiamted width of the scroll bar.
		local nWidth  = self.wndHandle:GetWidth()
		local nHeight = self.wndHandle:GetHeight()
		local nSize   = self.tItems[1]:GetSize()                   -- Assumes equal sized items.
		local nIpr    = math.floor(nWidth / nSize.nWidth)          -- Items Per Row
		local nIpc    = math.floor(nHeight / nSize.nHeight)        -- (Visible) Items Per Column
		local nPadH   = 0.5 * (nWidth - nSize.nWidth * nIpr - nSW) -- Horizontal padding
		local nPadV   = 0.5 * (nHeight - nSize.nHeight * nIpc)     -- Vertical padding (Not used now)
		
		for nIndex, tItem in pairs(self.tItems) do
			local nRow = math.floor((nIndex - 1) / nIpr)
			local nCol = (nIndex - 1) % nIpr
			
			-- Offsets for readability
			local nLeft   = nCol * (nSize.nWidth + nPadH)
			local nTop    = nRow * (nSize.nHeight - 4) 
			local nRight  = nLeft + nSize.nWidth 
			local nBottom = nTop + nSize.nHeight 
			 
			tItem:SetAnchor({
				tPoints  = {0, 0, 0, 0},
			    tOffsets = {nLeft, nTop, nRight, nBottom}
			})
		end
	end
end

function LootItemCollection:ContainsItemWithUUID(nUUID)
	for nIndex, tExistingItem in pairs(self.tItems) do
		if tExistingItem.nUUID == nUUID then
			return true	
		end
	end
	
	return false
end

function LootItemCollection:GetItemWithUUID(nUUID)
	for nIndex, tExistingItem in pairs(self.tItems) do
		if tExistingItem.nUUID == nUUID then
			return tExistingItem	
		end
	end
	
	return nil
end

function LootItemCollection:Lock()
	for nIndex, tItem in ipairs(self.tItems) do
		tItem:SetLock(true)
	end
end

function LootItemCollection:Unlock()
	for nIndex, tItem in ipairs(self.tItems) do
		tItem:SetLock(false)
	end
end

function LootItemCollection:SetCallback(tCallback)
	self.tCallback = tCallback
end

function LootItemCollection:GetSelectedItems()
	local tSelectedItems = {}
	for _, tItem in ipairs(self.tSelectedItems) do
		table.insert(tSelectedItems, tItem)
	end
	return tSelectedItems
end

function LootItemCollection:UnselectAll()
	self:RunInEventProtectedScope(function() 
		for nIndex, tSelectedItem in ipairs(self.tSelectedItems) do
			tSelectedItem:Unselect(true) -- Prevent events 
		end

		self.tSelectedItems = {}
	end)
end

function LootItemCollection:SelectFirst()
	if #self.tItems > 0 then
		self:RunInEventProtectedScope(function() 
			local tItemToSelect = self.tItems[1]
			self:UnselectAll()
			tItemToSelect:Select(true) -- Prevent event
			
			table.insert(self.tSelectedItems, tItemToSelect)
		end)
	end
end

function LootItemCollection:IsMultiSelectionAllowed()
	return Settings:ReadSetting("tLeader.bAllowMultiSelection") and Apollo.IsControlKeyDown()
end

function LootItemCollection:SetCompareFunction(fCompare)
	self.fCompare = fCompare
end

-- Protects against multiple event invocations during fRun. If an event is emitted it 
-- happens on the end. Note RunInEventProtectedScope may be called nested
function LootItemCollection:RunInEventProtectedScope(fRun, ...)
	local bShouldPreventEvent = self.bShouldPreventEvents	
	self.bShouldPreventEvents = true 
	
	-- Block all events generated by fRun.
	fRun(...)

	self.bShouldPreventEvents = bShouldPreventEvent
	
	-- Attempt fire an event. 
	self:FireEvent("OnLootItemSelectionChanged", self.tSelectedItems)
end

function LootItemCollection:FireEvent(eventName, ...)
	if (not self.bShouldPreventEvents) and self.tCallback ~= nil then
		local func = self.tCallback[eventName]
		
		if func ~= nil then
			-- IMPORTANT --
			-- Make sure to block all events that may be triggered by running 'func'	
			-- This will block potentail infinite recursion.
			self.bShouldPreventEvents = true 
			func(self.tCallback, ...)
			self.bShouldPreventEvents = false
		end
	end
end

function LootItemCollection:GetItemWithId(nItemId)
	for _, tItem in pairs(self.tItems) do
		if tItem:HaveId(nItemId) then
			return tItem
		end
	end
	
	return nil
end

function LootItemCollection:GetItem(tItem)
	for _, tExistingItem in pairs(self.tItems) do
		if tExistingItem:GetItem():GetItemId() == tItem:GetItemId() then
			return tExistingItem
		end
	end
	
	return nil
end

----------------------------------------------------------------------------------
-- Events
----------------------------------------------------------------------------------
function LootItemCollection:OnItemSelected(tItem)
	if self.bIsSelectable then 
		self:RunInEventProtectedScope(function() 
			if not self:IsMultiSelectionAllowed() then
				self:UnselectAll()
			end
	
			table.insert(self.tSelectedItems, tItem)
		end)
	end
end

function LootItemCollection:OnItemUnselected(tItem)
	if self.bIsSelectable then 
		self:RunInEventProtectedScope(function()
			if self:IsMultiSelectionAllowed() then
				for nIndex, tSelectedItem in ipairs(self.tSelectedItems) do
					if tSelectedItem == tItem then
						table.remove(self.tSelectedItems, nIndex)
						break
					end	
				end
			else 
				local wasMultiSelect = #self.tSelectedItems > 1 
			
				self:UnselectAll()
				
				if wasMultiSelect then
					tItem:Select(false)
				end
			end
		end)
	end
end

ClassFactory:RegisterClass("LootItemCollection", LootItemCollection)
