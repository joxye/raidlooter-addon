--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
-- Roller 
-- Represents a raider which have rolled for an item. 
--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
local Roller = {}

----------------------------------------------------------------------------------
-- Initialization
----------------------------------------------------------------------------------
function Roller:New(wndParent, bIsSelectable)
	local meta = {
		bIsSelectable  = bIsSelectable,
		strUnitName    = "",
		strDisplayName = "",
		nScore         = nil,
		nRank          = nil,
		nOrderValue    = 0,
		wndHandle      = nil,
		wndRank        = nil,
		wndRaider      = nil,
		wndScore       = nil,
		wndAnimation   = nil,
	}
	
    local self = ClassFactory:SetupInstance(meta, Roller)
	
	self.wndHandle    = XMLFactory:CreateWindow("Roller", wndParent, self)
	self.wndRank      = self.wndHandle:FindChild("Rank")
	self.wndRaider    = self.wndHandle:FindChild("Name")
	self.wndScore     = self.wndHandle:FindChild("Roll")
	self.wndAnimation = self.wndHandle:FindChild("LeaderAnimation")
	
	-- Events
	if self.bIsSelectable then
		--self.btn:AddEventHandler("ButtonSignal", "OnButtonPressed", self)
	end
	
    return self
end	

function Roller:Destroy()
	if self.wndHandle ~= nil then
		self.wndHandle:Destroy()
	end
end

----------------------------------------------------------------------------------
-- Functions
----------------------------------------------------------------------------------
function Roller:Set(nRank, strUnitName, nScore, nOrderValue)
	self.strUnitName = strUnitName
	self.nScore      = nScore
	self.nRank       = nRank
		
	self:SetOrderValue(nOrderValue)
	
	self.wndRank:SetText(self.nRank)
	self.wndScore:SetText(self.nScore)
	self.wndAnimation:Show(self.nRank == 1, true)
end

function Roller:SetOrderValue(nOrderValue)
	self.nOrderValue = 0
	
	if nOrderValue ~= nil then
		self.nOrderValue = nOrderValue
	end
	
	local strRollType = "" 
	if self.nOrderValue == 1 then
		strRollType = " (os)"
	elseif self.nOrderValue == 2 then
		strRollType = " (c)"
	end
	
	self.strDisplayName = self.strUnitName:gmatch("[^ ]+")()
	self.wndRaider:SetText(self.strDisplayName .. strRollType)
end

function Roller:SetAnchor(tAnchor)
	local tP = tAnchor.tPoints
	local tO = tAnchor.tOffsets

	self.wndHandle:SetAnchorPoints(tP[1], tP[2], tP[3], tP[4])
	self.wndHandle:SetAnchorOffsets(tO[1], tO[2], tO[3], tO[4])
end

function Roller:GetHeight()
	return self.wndHandle:GetHeight()
end

function Roller:SetRank(nRank)
	self.nRank = nRank
	self.wndRank:SetText(nRank)
	self.wndAnimation:Show(self.nRank == 1, true)
end

function Roller:IsNamed(strUnitName)
	return self.strUnitName == strUnitName
end

function Roller:GetScore()
	return self.nScore
end

function Roller:GetName()
	return self.strUnitName
end

function Roller:GetOrderValue()
	if self.nOrderValue == nil then
		return 0
	end
	
	return self.nOrderValue
end

----------------------------------------------------------------------------------
-- Events
----------------------------------------------------------------------------------
function Roller:OnButtonPressed(wndHandler, wndControl, eMouseButton, nLastRelativeMouseX, nLastRelativeMouseY)
	-- call somewhere.
end

ClassFactory:RegisterClass("Roller", Roller)