--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
-- ManagementWindow 
-- Represents the management window. This is the window that the leader/
-- assistents see.
--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
local ManagementWindow = {}

----------------------------------------------------------------------------------
-- Initialization
----------------------------------------------------------------------------------
function ManagementWindow:New()
	local meta = {
		wndMain = nil,
		tDistributionView = nil
	}

	local self = ClassFactory:SetupInstance(meta, ManagementWindow)
	
	self.wndMain = XMLFactory:CreateTopLevelWindow("Base", self)
	self:LoadTabs()
	
	Utils:MoveWindow(self.wndMain, Settings:ReadSetting("tWindowLocations.wndManageWindowLoc"))
	ChatParser:RegisterCallback(self)
	return self
end

function ManagementWindow:Destroy()
	ChatParser:UnregisterCallback(self)
end

----------------------------------------------------------------------------------
-- Save & Restore
----------------------------------------------------------------------------------
function ManagementWindow:OnSave(eLevel)
	Settings:WriteSetting("tWindowLocations.wndManageWindowLoc", self.wndMain:GetLocation():ToTable())
end

----------------------------------------------------------------------------------
-- Functions
----------------------------------------------------------------------------------
function ManagementWindow:LoadTabs()
	local bIsInRaid      = Utils:IsInRaid() and Utils:IsMasterLootingOn()
	local bIsDistributor = Utils:IsLeader() or Utils:IsAssistant()
	
	-- Setup each tab	
	self.tDistributionView = ClassFactory:CreateInstance("DistributionView", bIsDistributor, bIsInRaid)
	self.tDistributionView:Load(self.wndMain)	

	-- Currently the "Management" and "Info" tabs are just static and attached from here.
	local btnManagementTab = self.wndMain:FindChild("ManagementPageButton")
	local wndManagement = self.wndMain:FindChild("ManagementPage")
	btnManagementTab:AttachWindow(wndManagement)
	btnManagementTab:SetCheck(false)
	btnManagementTab:Enable(false)
	
	local btnInfoTab = self.wndMain:FindChild("InfoPageButton")
	local wndInfo = self.wndMain:FindChild("InfoPage")
	btnInfoTab:AttachWindow(wndInfo)
	btnInfoTab:SetCheck(false)
	btnInfoTab:Enable(false)
end

function ManagementWindow:UnloadTabs()
	if self.tDistributionView ~= nil then
		self.tDistributionView:Destroy()
	end
	
	self.tDistributionView = nil
end

function ManagementWindow:Hide(bImmediate)
	if bImmediate == nil then
		bImmediate = true
	end
	
	self.wndMain:Show(false, bImmediate) 
end

function ManagementWindow:Show(bImmediate)
	if bImmediate == nil then
		bImmediate = true
	end
	
	self.wndMain:Show(true, bImmediate)
end

function ManagementWindow:GetWindow()
	return self.wndMain
end

function ManagementWindow:CloseOptions()
	self.tOptions.wnd:Close()
	Utils:PlaySound(191)
end

function ManagementWindow:OpenOptions()
 	Utils:PlaySound(190)
end

function ManagementWindow:IsShown()
	return self.wndMain:IsShown()
end

function ManagementWindow:Close()
	Utils:PlaySound(191)
	return self.wndMain:Close()
end

function ManagementWindow:Open()
	return self.wndMain:Invoke()
end

function ManagementWindow:HandleOnTabChecked()
	Utils:PlaySound(184)
end

function ManagementWindow:HandleOnManagementWindowClose()
	self:Close()
end

----------------------------------------------------------------------------------
-- Events
----------------------------------------------------------------------------------
function ManagementWindow:OnTabChecked()
	Utils:InvokeEventHandler(self, "ManagementWindow", "OnTabChecked")
end
function ManagementWindow:OnManagementWindowClose(...)
	Utils:InvokeEventHandler(self, "ManagementWindow", "OnManagementWindowClose", ...)
end
function ManagementWindow:OnLootAssigned(...)
    Utils:FireEvent(self.tDistributionView, "OnLootAssigned", ...)
end
function ManagementWindow:OnLootUpdate(...)
	Utils:FireEvent(self.tDistributionView, "OnLootUpdate", ...)
end
function ManagementWindow:OnAssignLoot(...)
	Utils:FireEvent(self.tDistributionView, "OnAssignLoot", ...)
end
function ManagementWindow:OnBeginRoll(...)
	Utils:FireEvent(self.tDistributionView, "OnBeginRoll", ...)
end
function ManagementWindow:OnEndRoll(...)
	Utils:FireEvent(self.tDistributionView, "OnEndRoll", ...)
end
function ManagementWindow:OnRandomizeLoot(...)
	Utils:FireEvent(self.tDistributionView, "OnRandomizeLoot", ...)
end
function ManagementWindow:OnReset(...)
	Utils:FireEvent(self.tDistributionView, "OnReset", ...)
end
function ManagementWindow:OnReroll(...)
	Utils:FireEvent(self.tDistributionView, "OnReroll", ...)
end
function ManagementWindow:OnFilterToggle(...)
	Utils:FireEvent(self.tDistributionView, "OnFilterToggle", ...)
end
function ManagementWindow:OnCurrentRecipientDeselect(...)
	Utils:FireEvent(self.tDistributionView, "OnCurrentRecipientDeselect", ...)
end
function ManagementWindow:OnCurrentItemDeselect(...)
	Utils:FireEvent(self.tDistributionView, "OnCurrentItemDeselect", ...)
end
function ManagementWindow:OnClearLatestRolles(...)
	Utils:FireEvent(self.tDistributionView, "OnClearLatestRolles", ...)
end
function ManagementWindow:OnRoll_ChatParser(...)
	Utils:FireEvent(self.tDistributionView, "OnRoll_ChatParser", ...)
end
function ManagementWindow:OnBeginRoll_ChatParser(...)
	Utils:FireEvent(self.tDistributionView, "OnBeginRoll_ChatParser", ...)
end
function ManagementWindow:OnEndRoll_ChatParser(...)
	Utils:FireEvent(self.tDistributionView, "OnEndRoll_ChatParser", ...)
end
function ManagementWindow:OnRollHint_ChatParser(...)
	Utils:FireEvent(self.tDistributionView, "OnRollHint_ChatParser", ...)
end
function ManagementWindow:OnParsingStarted_ChatParser(...)
	Utils:FireEvent(self.tDistributionView, "OnParsingStarted_ChatParser", ...)
end
function ManagementWindow:OnParsingStoped_ChatParser(...)
	Utils:FireEvent(self.tDistributionView, "OnParsingStoped_ChatParser", ...)
end

ClassFactory:RegisterClass("ManagementWindow", ManagementWindow)