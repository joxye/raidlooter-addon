--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
-- LootWindow 
-- Represents the loot window i.e. the window everyone can see.
--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
local LootWindow = {}
local RollTypeEnum = {
	MainSpec = 1,
	OffSpec  = 2,
	Costume  = 3 
}
----------------------------------------------------------------------------------
-- Initialization
----------------------------------------------------------------------------------
function LootWindow:New()
	local meta = {
		wndOverlay    = nil,
		wndMain       = nil,
		wndRollerList = nil,
		wndLoot       = nil,
		wndAmount     = nil,
		wndLootItemsRegion = nil,
		wndLootRollRegion = nil,
		wndRollOutset = nil,
		tItemDialog   = nil,
		tInlineItem   = {
			wnd        = nil,
			wndIcon    = nil,
	        wndName    = nil,
			btnMain    = nil,
			btnOff     = nil,
			btnCostume = nil, 
			btnRoll    = nil,
			btnPass    = nil,
			eRollType  = RollTypeEnum.MainSpec,
		},
		tOptions = {
			wnd = nil,
			btn = nil
		},
		btnManage       = nil,
		btnExpand       = nil,
		btnRoll         = nil,
		btnTab          = nil,
		bIsRolling      = false,
		tRollSet        = nil, 
		tLootCollection = nil,
		tUpdateManagementButtonTimer = nil,
		nPreviousLootCount = 0
	}

	local self = ClassFactory:SetupInstance(meta, LootWindow)
	
	self.wndMain                = XMLFactory:CreateTopLevelWindow("LootView", self)
	self.wndOverlay             = XMLFactory:CreateTopLevelWindow("Overlay", self)
	self.wndLootItemsRegion     = self.wndMain:FindChild("LootItemsRegion")
	self.wndLootRollRegion      = self.wndMain:FindChild("LootRollRegion")
	self.wndRollOutset          = self.wndMain:FindChild("RollOutset")
	self.wndLoot                = self.wndMain:FindChild("UndistributedLootContainer") 
	self.wndRollerList          = self.wndMain:FindChild("Loot_RollTopList")
	self.btnExpand              = self.wndMain:FindChild("ExpandButton")
	self.btnManage              = self.wndMain:FindChild("ManageButton")
	self.tInlineItem.wnd        = self.wndMain:FindChild("InlineItemBase") 
	self.tInlineItem.wndItem    = self.wndMain:FindChild("LootInlineItemRoller")
	self.tInlineItem.wndName    = self.wndMain:FindChild("ItemRolledForName") 
	self.tInlineItem.wndIcon    = self.wndMain:FindChild("ItemRolledForIcon") 
	self.tInlineItem.btnMain    = self.wndMain:FindChild("Inline_MainSpecButton") 
	self.tInlineItem.btnOff     = self.wndMain:FindChild("Inline_OffSpecButton") 
	self.tInlineItem.btnCostume = self.wndMain:FindChild("Inline_CostumeButton") 
	self.tInlineItem.btnRoll    = self.wndMain:FindChild("InlineRollButton")
	self.tInlineItem.btnPass    = self.wndMain:FindChild("InlinePassButton")
	self.tOptions.wnd           = self.wndMain:FindChild("OptionsDialog")
	self.tOptions.btn           = self.wndMain:FindChild("ConfigureButton")
	self.wndAmount              = self.wndOverlay:FindChild("Amount")
	self.tRollSet               = ClassFactory:CreateInstance("RollerCollection", true, self.wndRollerList)
	self.tLootCollection        = ClassFactory:CreateInstance("LootItemCollection", true, false, self.wndLoot)
	
	self.btnExpand:AttachWindow(self.wndRollOutset)
	self.btnExpand:SetCheck(false)
	self.btnManage:Show(false, true)
	self.tOptions.btn:AttachWindow(self.tOptions.wnd)
	self.tOptions.btn:SetCheck(false)
	self.tInlineItem.wnd:Show(false)
	self.tInlineItem.btnMain:SetCheck(true)
	
	self.tLootCollection:SetCompareFunction(function(tItem) 
		return tItem:GetEquippedItemForItemType() 
	end)
		
	-- Read from settings.
	self.wndMain:FindChild("opt-btn_tGeneral_bUseRollPopups"):SetCheck(Settings:ReadSetting("tGeneral.bUseRollPopups"))
	self.wndMain:FindChild("opt-btn_tGeneral_bOpenOnLoot"):SetCheck(Settings:ReadSetting("tGeneral.bOpenOnLoot"))
	self.wndMain:FindChild("opt-btn_tGeneral_bUseOverlay"):SetCheck(Settings:ReadSetting("tGeneral.bUseOverlay"))
	self.wndMain:FindChild("opt-btn_tGeneral_bUseSound"):SetCheck(Settings:ReadSetting("tGeneral.bUseSound"))
	self.wndMain:FindChild("opt-btn_tGeneral_bUseRollSound"):SetCheck(Settings:ReadSetting("tGeneral.bUseRollSound"))
	self.wndMain:FindChild("opt-btn_tGeneral_bUseLootSound"):SetCheck(Settings:ReadSetting("tGeneral.bUseLootSound"))
	
	Utils:MoveWindow(self.wndOverlay, Settings:ReadSetting("tWindowLocations.wndOverlayLoc"))
	Utils:MoveWindow(self.wndMain, Settings:ReadSetting("tWindowLocations.wndLootWindowLoc"))
	
	ChatParser:RegisterCallback(self)
	return self
end

function LootWindow:Destroy()
	ChatParser:UnregisterCallback(self)
end

----------------------------------------------------------------------------------
-- Save & Restore
----------------------------------------------------------------------------------
function LootWindow:OnSave(eLevel)
	if self.wndOverlay ~= nil then
		Settings:WriteSetting("tWindowLocations.wndOverlayLoc", self.wndOverlay:GetLocation():ToTable())
	end

	Settings:WriteSetting("tWindowLocations.wndLootWindowLoc", self.wndMain:GetLocation():ToTable())
end

-----------------------------------------------------------------------------------------------
-- Functions
-----------------------------------------------------------------------------------------------
function LootWindow:FindLootItem(tItem)
	if self.tLootCollection ~= nil then
		return self.tLootCollection:GetItem(tItem)
	end 
	
	return nil
end

function LootWindow:Hide(bImmediate)
	if bImmediate == nil then
		bImmediate = true
	end
	
	self.wndMain:Show(false, bImmediate) 
end

function LootWindow:Show(bImmediate )
	if bImmediate == nil then
		bImmediate = true
	end
	
	self.wndMain:Show(true, bImmediate)
end

function LootWindow:GetWindow()
	return self.wndMain
end

function LootWindow:IsShown()
	return self.wndMain:IsShown()
end

function LootWindow:Close()
	Utils:PlaySound(191)
	self.btnExpand:SetCheck(false)
	self.wndMain:Close()
end

function LootWindow:Open()
	Utils:PlaySound(190)
	self.wndMain:Invoke()
	self:ShowManagementButton(Utils:IsLeader())
	self.tUpdateManagementButtonTimer = ApolloTimer.Create(1, true, "OnUpdateManagementButton", self)
end

function LootWindow:ShowManagementButton(bVisible)
	local strDBG = ""
	if bVisible then
		strDBG = "The management button *SHOULD BE* visible."
	else
		strDBG = "The management button *SHOULD BE* hidden."
	end

	Utils:Log("ShowManagementButton => "..strDBG)

	self.btnManage:Show(bVisible)
end

-----------------------------------------------------------------------------------------------
-- Event handlers
-----------------------------------------------------------------------------------------------
function LootWindow:HandleOnUpdateManagementButton()
	self:ShowManagementButton(Utils:IsLeader())
	self.tUpdateManagementButtonTimer:Stop()
	self.tUpdateManagementButtonTimer = nil
end

function LootWindow:HandleOnOpenManagementWindow(wndHandler, wndControl, eMouseButton)
	local tManagementWindow = RaidLooter:GetManagementWindow()
	if tManagementWindow ~= nil then
		if tManagementWindow:IsShown() then
			tManagementWindow:Close()
		else 
			tManagementWindow:Open()
		end
	end
end

function LootWindow:HandleOnToggleWindows()
	RaidLooter:ToggleWindows()
end

function LootWindow:HandleOnCloseOptions()
	Utils:PlaySound(191)
end

function LootWindow:HandleOnOpenOptions()
	self.btnExpand:SetCheck(false)
 	Utils:PlaySound(190)
end

function LootWindow:HandleOnOptionUnselected(wndHandler, wndControl, eMouseButton)
	self:OptionSelectionHelper(wndHandler:GetName(), false)
end

function LootWindow:HandleOnOptionSelected(wndHandler, wndControl, eMouseButton)
	self:OptionSelectionHelper(wndHandler:GetName(), true)
end

function LootWindow:HandleOnLootUpdate(tLoot)
	local bOpenOnLoot = Settings:ReadSetting("tGeneral.bOpenOnLoot")
	local bUseOverlay = Settings:ReadSetting("tGeneral.bUseOverlay")
	self.tLootCollection:UpdateLoot(tLoot)

	if bOpenOnLoot then
		local nLootCount = #tLoot
	
		if nLootCount > 0 then
			if bUseOverlay then
				self.wndAmount:SetText(nLootCount)
				self.wndOverlay:Show(true)
			end
			
			if nLootCount > self.nPreviousLootCount then
				Utils:PlayNewLootSound()
				
				if not bUseOverlay then
					self:Open()
				end
			end
		else
			self.wndOverlay:Show(false)	
			
			if self.nPreviousLootCount > 0 then
				self:Close()
			end
		end
			
		self.nPreviousLootCount = nLootCount	
	else 
		self.wndOverlay:Show(false)
	end
end

function LootWindow:HandleOnClearLatestRolles()
	if self.tRollSet ~= nil then
		self.tRollSet:Clear()
	end
end

function LootWindow:HandleOnRoll_ChatParser(strUnitName, nScore)
	if self.bIsRolling then
		if self.tRollSet:IsEmpty() then
			self.btnExpand:SetCheck(true)
		end
	
		self.tRollSet:AddRoller(strUnitName, nScore)
	end
	
	if strUnitName == Utils:GetPlayerName() then
		self:DisableInlineItem()

		if self.tItemDialog ~= nil then
			self.tItemDialog:Destroy()
			self.tItemDialog = nil
		end
	end
end

function LootWindow:HandleOnBeginRoll_ChatParser(strUnitName, tItem)
	if self.bIsRolling then
		self:HandleOnEndRoll_ChatParser(strUnitName)
	end
	
	local tLootItem = self:FindLootItem(tItem)
	if tLootItem ~= nil then
		self.tRollSet:Clear()
		self.tRollSet:BeginRollingForItem(tLootItem)
		self.bIsRolling = true
		
		if Settings:ReadSetting("tGeneral.bUseRollPopups") then
			self:CreateRollForItemDialog(tItem)
		else
			self:ShowInlineItem(tLootItem)
		end
	else
		Utils:Log("Unable to find loot item for id {" .. tItem:GetItemId() .. "}")
	end
end

function LootWindow:HandleOnEndRoll_ChatParser(strUnitName)
	self.tRollSet:EndRolling()
	self.bIsRolling = false
	
	self:ResetInlineItem()
	
	if self.tItemDialog ~= nil then
		self.tItemDialog:Destroy()
		self.tItemDialog = nil
	end
end

function LootWindow:HandleOnRollHint_ChatParser(strUnitName, tHint)
	self.tRollSet:UpdateRollHint(strUnitName, tHint)

	if strUnitName== Utils:GetPlayerName() then
		if tHint.bIsOffspec then 
			self.eRollType = RollTypeEnum.OffSpec
			self.tInlineItem.btnOff:SetCheck(true)
		elseif tHint.bIsCostume then
			self.eRollType = RollTypeEnum.Costume
			self.tInlineItem.btnCostume:SetCheck(true)
		end
	end
end

function LootWindow:HandleOnMakeRoll(wndHandler, wndControl, eMouseButton)
	ChatSystemLib.Command("/roll")
	local nChNum   = Debug.bIsTest and ChatSystemLib.ChatChannel_Say or ChatSystemLib.ChatChannel_Party	
	local tChannel = ChatSystemLib.GetChannels()[nChNum]
	
	if self.eRollType == RollTypeEnum.OffSpec then
		tChannel:Send("os")
	elseif self.eRollType == RollTypeEnum.Costume then
		tChannel:Send("costume")
	end

	self:DisableInlineItem()
	Utils:PlaySound(174)
end

function LootWindow:HandleOnMakePass(wndHandler, wndControl, eMouseButton)
	self:DisableInlineItem()
	Utils:PlaySound(200)
end

function LootWindow:HandleOnToggleLatestRolls(wndHandler, wndControl, eMouseButton)
	self.tOptions.btn:SetCheck(false)
end

function LootWindow:HandleOnLootWindowOpen(wndHandler, wndControl, eMouseButton)
	self:Open()
end

function LootWindow:HandleOnLootWindowClose(wndHandler, wndControl, eMouseButton)
	self:Close()
end

function LootWindow:HandleOnOptionUnselected(wndHandler, wndControl, eMouseButton)
	self:OptionSelectionHelper(wndHandler:GetName(), false)
end

function LootWindow:HandleOnOptionSelected(wndHandler, wndControl, eMouseButton)
	self:OptionSelectionHelper(wndHandler:GetName(), true)
end

function LootWindow:HandleOnInlinePopupClose(wndHandler, wndControl, eMouseButton)
	self:DisableInlineItem()
end

function LootWindow:HandleOnInlineRollTypeChanged(wndHandler, wndControl, eMouseButton)
	if wndControl:IsChecked() then
		local strButtonName = wndControl:GetName()
		 
		if strButtonName == "Inline_MainSpecButton" then
			self.eRollType = RollTypeEnum.MainSpec
		elseif strButtonName == "Inline_OffSpecButton" then
			self.eRollType = RollTypeEnum.OffSpec
		elseif strButtonName == "Inline_CostumeButton" then
			self.eRollType = RollTypeEnum.Costume
		else
			self.eRollType = RollTypeEnum.MainSpec
		end
	else
		self.eRollType = RollTypeEnum.MainSpec
	end
end

-----------------------------------------------------------------------------------------------
-- Events
-----------------------------------------------------------------------------------------------
function LootWindow:OnUpdateManagementButton(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnUpdateManagementButton", ...)
end
function LootWindow:OnOpenManagementWindow(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnOpenManagementWindow", ...)
end
function LootWindow:OnToggleWindows(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnToggleWindows", ...)
end
function LootWindow:OnCloseOptions(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnCloseOptions", ...)
end
function LootWindow:OnOpenOptions(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnOpenOptions", ...)
end
function LootWindow:OnLootUpdate(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnLootUpdate", ...)
end
function LootWindow:OnClearLatestRolles(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnClearLatestRolles", ...)
end
function LootWindow:OnRoll_ChatParser(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnRoll_ChatParser", ...)
end
function LootWindow:OnBeginRoll_ChatParser(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnBeginRoll_ChatParser", ...)
end
function LootWindow:OnEndRoll_ChatParser(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnEndRoll_ChatParser", ...)
end
function LootWindow:OnRollHint_ChatParser(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnRollHint_ChatParser", ...)
end
function LootWindow:OnMakeRoll(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnMakeRoll", ...)
end
function LootWindow:OnMakePass(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnMakePass", ...)
end
function LootWindow:OnClearRolles_Loot(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnClearLatestRolles", ...)
end
function LootWindow:OnToggleLatestRolls(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnToggleLatestRolls", ...)
end
function LootWindow:OnLootWindowOpen(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnLootWindowOpen", ...)
end
function LootWindow:OnLootWindowClose(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnLootWindowClose", ...)
end
function LootWindow:OnOptionUnselected(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnOptionUnselected", ...)
end
function LootWindow:OnOptionSelected(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnOptionSelected", ...)
end
function LootWindow:OnInlinePopupClose(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnInlinePopupClose", ...)
end
function LootWindow:OnInlineRollTypeChanged(...)
	Utils:InvokeEventHandler(self, "LootWindow", "OnInlineRollTypeChanged", ...)
end

----------------------------------------------------------------------------------
-- Private functions
----------------------------------------------------------------------------------
function LootWindow:CreateRollForItemDialog(tItem)
	if self.tItemDialog ~= nil then
		self.tItemDialog:Destroy()
	end
	
	for _, tLootItem in ipairs(RaidLooter.tLoot) do
		if tLootItem.itemDrop:GetItemId() == tItem:GetItemId() then
			self.tItemDialog = ClassFactory:CreateInstance("ItemRollDialog", tLootItem)
			
			local tSize = self.tItemDialog:GetSize()
			local nWidthOverTwo  = 0.5 * tSize.nWidth
			local nHeightOverTwo = 0.5 * tSize.nHeight
			
			self.tItemDialog:SetAnchor({
				tPoints  = {0.5, 0.30, 0.5, 0.30},
				tOffsets = {-nWidthOverTwo, -nHeightOverTwo, nWidthOverTwo, nHeightOverTwo }
			})
			break
		end
	end
end

function LootWindow:ShowInlineItem(tLootItem)
	local strTextColor = Utils:GetItemQualityString(tLootItem)
	
	self.tInlineItem.wnd:Show(true)
	self.tInlineItem.wndName:SetText(tLootItem:GetName())
	self.tInlineItem.wndName:SetTextColor(strTextColor)
	self.tInlineItem.wndIcon:SetSprite(tLootItem:GetIcon())
	self.tInlineItem.btnMain:SetCheck(true)
	self.tInlineItem.btnOff:SetCheck(false)
	self.tInlineItem.btnCostume:SetCheck(false)
	
	local tItem         = tLootItem:GetItem()
	local tItemEquipped = tItem:GetEquippedItemForItemType()
	
	Tooltip.GetItemTooltipForm(self, self.tInlineItem.wndItem, tItem, {
		bPrimary = true, 
		bSelling = false, 
		itemCompare = tItemEquipped
	})
	
	local bIsNotInline = Settings:ReadSetting("tGeneral.bUseRollPopups")
	
	if bIsNotInline then
		self:DisableInlineItem()
	else
		Utils:PlayNewRollSound()
	end
end

function LootWindow:ResetInlineItem()
	self.tInlineItem.btnRoll:Enable(true)
	self.tInlineItem.btnPass:Enable(true)
	self.tInlineItem.btnMain:Enable(true)
	self.tInlineItem.btnOff:Enable(true)
	self.tInlineItem.btnCostume:Enable(true)
	self.tInlineItem.wnd:Show(false, false)
	self.eRollType = RollTypeEnum.MainSpec
end

function LootWindow:DisableInlineItem()
	self.tInlineItem.btnRoll:Enable(false)
	self.tInlineItem.btnPass:Enable(false)
	self.tInlineItem.btnMain:Enable(false)
	self.tInlineItem.btnOff:Enable(false)
	self.tInlineItem.btnCostume:Enable(false)
	self.tInlineItem.wnd:Show(false, false)
	self.eRollType = RollTypeEnum.MainSpec
end

function LootWindow:OptionSelectionHelper(strName, bIsChecked)
	local strControl, strSection, strVarName = Utils:Split(strName, "_")
	
	if strControl == "opt-btn" then
		if strSection ~= nil and strVarName ~= nil then
			self.wndMain:FindChild(strName):SetCheck(bIsChecked)
			Settings:WriteSetting(strSection.."."..strVarName, bIsChecked)
			self:OptionSelectionLinkerHelper(strName, bIsChecked)
						
			if (strName == "opt-btn_tGeneral_bUseOverlay") then
				if bIsChecked then
					local nLootCount = #RaidLooter.tLoot
					
					if nLootCount > 0 then
						self.wndAmount:SetText(nLootCount)
						self.wndOverlay:Show(true)
					end
				else
					self.wndOverlay:Show(false)
				end
			end
		end
	end
end

function LootWindow:OptionSelectionLinkerHelper(strName, bIsChecked)
	if strName == "opt-btn_tGeneral_bOpenOnLoot" then
		local btnOverlay = self.wndMain:FindChild("opt-btn_tGeneral_bUseOverlay")
		
		self:OptionSelectionButtonLinkerHelper(btnOverlay, bIsChecked, "tGeneral", "bUseOverlay")
		
		if not bIsChecked then
			self.wndOverlay:Show(false)
		end
	elseif strName == "opt-btn_tGeneral_bUseSound" then
		local btnRoll = self.wndMain:FindChild("opt-btn_tGeneral_bUseRollSound")
		local btnLoot = self.wndMain:FindChild("opt-btn_tGeneral_bUseLootSound")
		
		self:OptionSelectionButtonLinkerHelper(btnRoll, bIsChecked, "tGeneral", "bUseRollSound")
		self:OptionSelectionButtonLinkerHelper(btnLoot, bIsChecked, "tGeneral", "bUseLootSound")
	end
end

function LootWindow:OptionSelectionButtonLinkerHelper(btn, bIsChecked, strSection, strOption)
	if bIsChecked then
		btn:Enable(true)
		btn:SetTextColor("UI_TextHoloTitle")
		btn:SetBGColor("ffffffff")
	else 
		btn:SetTextColor("6f909090")
		btn:SetBGColor("4fffffff")
		btn:Enable(false)
		btn:SetCheck(false)
		Settings:WriteSetting(strSection.."."..strOption, false)
	end
end

ClassFactory:RegisterClass("LootWindow", LootWindow)