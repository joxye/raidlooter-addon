--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
-- RollerCollection
-- Represents a unique collection of rollers that are eligible recipients for a 
-- specific undistributed item. 
--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
local RollerCollection = {}

----------------------------------------------------------------------------------
-- Initialization
----------------------------------------------------------------------------------
function RollerCollection:New(bIsSelectable, wndTarget)
	local meta = {	
		wndHandle     = wndTarget,
		nLastRank     = 0,
		tCallback     = nil,
		tRollers      = {},
		tRollHints    = {},
		tLootItem     = nil,
		bIsParsing    = false,
		bIsSelectable = bIsSelectable,
	}
	
    return ClassFactory:SetupInstance(meta, RollerCollection)

end	

function RollerCollection:Destroy()
	for _, tRoller in pairs(self.tRollers) do
		tRoller:Destroy()
	end
end

----------------------------------------------------------------------------------
-- Functions
----------------------------------------------------------------------------------
function RollerCollection:BeginRollingForItem(tLootItem)
	if not self.bIsParsing and tLootItem ~= nil then
		self.tLootItem  = tLootItem
		self.bIsParsing = true
		self.tRollHints = {} -- Reset hints.
	end
end

function RollerCollection:EndRolling()
	if self.bIsParsing then
		self.tLootItem  = nil
		self.bIsParsing = false
	end
end

function RollerCollection:IsEmpty()
	return self.tRollers == nil or #self.tRollers == 0
end

function RollerCollection:SetCallback(tCallback)
	self.tCallback = tCallback
end

function RollerCollection:Clear()
	for _, tRoller in pairs(self.tRollers) do
		tRoller:Destroy()
	end
	
	self.tRollers  = {}
	self.nLastRank = 0
end

function RollerCollection:Contains(strUnitName) 
	return self:GetRoller(strUnitName) ~= nil 
end

function RollerCollection:GetRoller(strUnitName)
	for _, tRoller in pairs(self.tRollers) do
		if tRoller:IsNamed(strUnitName) then
			return tRoller
		end
	end
	
	return nil
end

function RollerCollection:UpdateRollHint(strUnitName, tHint)
	Utils:Log("UpdateRollHint {" .. strUnitName .. "}")
	if strUnitName ~= nil and tHint ~= nil then
		local nOrderValue = 0
		local tRoller     = self:GetRoller(strUnitName)
		
		if tHint.bIsOffspec then
			nOrderValue = 1
		elseif tHint.bIsCostume then
			nOrderValue = 2
		end
		
		self.tRollHints[strUnitName] = nOrderValue

		if tRoller ~= nil then
			tRoller:SetOrderValue(nOrderValue)
		end
	end
end

function RollerCollection:AddRoller(strUnitName, nScore)
	if Debug.bIsTest or not self:Contains(strUnitName) then
		self.nLastRank = self.nLastRank + 1
		
		-- Insert at the correct position
		local nRank       = 1
		local nCmpScore   = nil
		local nOrderValue = self.tRollHints[strUnitName]
		
		for _, tCmpRoller in pairs(self.tRollers) do
			nCmpScore = tCmpRoller:GetScore()
			if nScore > nCmpScore then
				if nRank == 1 then
					Utils:FireEvent(self.tCallback, "OnRollerEvent_NewLeader", nRank, strUnitName, nScore)
				end
	
				table.insert(self.tRollers, nRank, self:CreateNewRoller(nRank, strUnitName, nScore, nOrderValue))
				break
			elseif nScore == nCmpScore and nRank == 1 then
				-- It is a tie
				Utils:FireEvent(self.tCallback, "OnRollerEvent_Tied", nRank, strUnitName, nScore)
			end
			
			nRank = nRank + 1
		end
		
		if nRank == self.nLastRank then
			-- Check if new leader
			if nRank == 1 then
				Utils:FireEvent(self.tCallback, "OnRollerEvent_NewLeader", nRank, strUnitName, nScore)
			end
		
			local tRoller = self:CreateNewRoller(self.nLastRank, strUnitName, nScore, nOrderValue)
			table.insert(self.tRollers, tRoller)
			self.nRollerHeight = tRoller:GetHeight()
		end
		
		self:ArrangeFromRank(nRank)
		self.wndHandle:RecalculateContentExtents()
	end
end

----------------------------------------------------------------------------------
-- Private functions
----------------------------------------------------------------------------------
function RollerCollection:CreateNewRoller(nRank, strUnitName, nScore, nOrderValue)
	local newRoller = ClassFactory:CreateInstance("Roller", self.wndHandle, self.bIsSelectable)
	newRoller:Set(nRank, strUnitName, nScore, nOrderValue)
	return newRoller
end

-- This function also updates the ranks ...
function RollerCollection:ArrangeFromRank(nStartRank)
	local nLeft   =  2
	local nTop    =  0
	local nRight  = -2 
	local nBottom =  0
	local nPad    =  1

	for nRank = nStartRank, self.nLastRank do 
		local tRoller = self.tRollers[nRank]
		tRoller:SetRank(nRank) 
		
		nTop    = (nRank - 1) * (self.nRollerHeight + nPad)
		nBottom = nTop + self.nRollerHeight
		
		tRoller:SetAnchor({
			tPoints  = {0, 0, 1, 0},
		    tOffsets = {nLeft, nTop, nRight, nBottom}
		})
	end
end

function RollerCollection:GetSummaryString()
	local tTopRoller, bIsTie = self:GetDistictTop()

	if tTopRoller ~= nil then
		return "Best roll: " .. tTopRoller:GetName() .. " (" .. tTopRoller:GetScore() .. ")."
	elseif bIsTie then  
		return "Tie between: " .. self:FormatTopTie()
	end
	
	return "No rolls."
end

function RollerCollection:FormatTopTie()
	local nRollers = #self.tRollers
	local strResult   = ""
	local nTieScore = 0
	
	if nRollers == 0 then
		return strResult
	end
	
	local tTieBetween = {}
	for nIndex, tRoller in ipairs(self.tRollers) do 
		if nIndex == 1 then
			nTieScore = tRoller:GetScore()
		elseif nTieScore ~= tRoller:GetScore() then 
			break
		end
	
		table.insert(tTieBetween, tRoller)
	end
	
	nRollers = #tTieBetween
	for nIndex, tRoller in ipairs(tTieBetween) do
		strResult = strResult .. tRoller:GetName()
		
		if nIndex ~= nRollers then
			if nIndex == nRollers - 1 then
				strResult = strResult .. " and "
			else 
				strResult = strResult .. ", "
			end
		end	
	end
	
	strResult = strResult .. " (" .. nTieScore .. ")."
	return strResult
end

-- Note --
-- Returns two variables 
--     1. The top roller if any 
--     2. If the lead is tied.
--
-- The top roller is the highest main-spec roll followed by 
-- off-spec and last costume rolls.
function RollerCollection:GetDistictTop()
	local tRollers = {}
	local tPrevRoller = nil
	
	for nOrder = 0, 2 do 
		for _, tRoller in ipairs(self.tRollers) do
			if tRoller:GetOrderValue() == nOrder then
				if tPrevRoller ~= nil and tPrevRoller:GetScore() ~= tRoller:GetScore() then
					break
				end
			
				table.insert(tRollers, tRoller)
				tPrevRoller = tRoller
			end
		end
		
		if #tRollers > 0 then
			return self:SelectDistinctTop(tRollers)
		end
	end
	
	return nil, false
end

function RollerCollection:SelectDistinctTop(tRollers)
	local nRollers = #tRollers
	
	if nRollers == 0 then	
		return nil, false
	elseif nRollers == 1 then
		return tRollers[1], false
	elseif tRollers[1]:GetScore() > tRollers[2]:GetScore() then
		return tRollers[1], false
	else 
		-- Tied because the sequence is ordered.
		return nil, true
	end
end

----------------------------------------------------------------------------------
-- Events
----------------------------------------------------------------------------------
function RollerCollection:OnClearButtonPressed(wndHandler, wndControl, eMouseButton)
	self:Clear()
end

ClassFactory:RegisterClass("RollerCollection", RollerCollection)