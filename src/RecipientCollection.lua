--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
-- RecipientCollection
-- Represents a collection of raiders that are eligible recipients for a 
-- specific undistributed item. 
--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
local RecipientCollection = {}

----------------------------------------------------------------------------------
-- Initialization
----------------------------------------------------------------------------------
function RecipientCollection:New(wndTarget) 
	local meta = {
		nMaxRows         = 10,
		nColumns         = 2,
		wndHandle        = wndTarget,
		tRecipients      = {},
		tSelection       = nil,
		tInspector       = nil,
		tCallback        = nil,
		strCurrentFilter = "A",
	}
	
	return ClassFactory:SetupInstance(meta, RecipientCollection)
end

function RecipientCollection:Destroy()
	for _, tRecipient in pairs(self.tRecipients) do
		tRecipient:Destroy()
	end
end

----------------------------------------------------------------------------------
-- Functions
----------------------------------------------------------------------------------
function RecipientCollection:Clear()
	self.tSelection = nil
	
	for _, tRecipient in pairs(self.tRecipients) do
		tRecipient:Destroy()
	end

	self.tRecipients = {}
end

function RecipientCollection:FindRecipientByName(strName)
	for _, tRecipient in pairs(self.tRecipients) do
		if tRecipient:IsNamed(strName) then
			return tRecipient
		end
	end	
	
	return nil
end

function RecipientCollection:Update(tEligibleRecipients, bShouldKeepSelection)
	-- Copy all current recipients.
	local tKillRecipients = {}
	for _, tRecipients in ipairs(self.tRecipients) do
		tKillRecipients[tRecipients:GetFullname()] = tRecipients 
	end

	-- Insert/update recipients.
	for _, tLooterInfo in pairs(tEligibleRecipients) do
		if tLooterInfo.strUnitName ~= nil and tLooterInfo.strUnitName ~= "" then
			local tExistingRecipient = self:FindRecipientByName(tLooterInfo.strUnitName)
			
			if tExistingRecipient ~= nil then
				local bIsOutOfRange = tLooterInfo.tUnit == nil or not tLooterInfo.tUnit:IsValid()
				tExistingRecipient:SetIsOutOfRange(bIsOutOfRange)
				
				if bIsOutOfRange then
					if tExistingRecipient:IsSelected() then
						tExistingRecipient:Unselect()
						tExistingRecipient:StopInspection()
					end
				else
					tExistingRecipient:SetUnit(tLooterInfo.tUnit)
				end

				tKillRecipients[tExistingRecipient:GetFullname()] = nil
			else 
				self:AddRecipient(ClassFactory:CreateInstance("Raider", self.wndHandle, tLooterInfo.tUnit, tLooterInfo.strUnitName))
			end
		end
	end
	
	-- Remove remaining recipients.
	for nKey, tRecipients in pairs(tKillRecipients) do
		self:RemoveRecipient(tRecipients)
	end
	
	self:ArrangeAndFilter()
	
	if bShouldKeepSelection == nil or not bShouldKeepSelection then
		self:UnselectCurrent()
	end
end

function RecipientCollection:ApplyFilter()
	self:ArrangeAndFilter()
end

function RecipientCollection:AddRecipient(tRecipient)
	tRecipient:SetCallback(self)
	table.insert(self.tRecipients, tRecipient)
end

function RecipientCollection:RemoveRecipient(tRecipient)
	if tRecipient == self.tSelection then
		self.tSelection = nil 
	end

	for nIndex, tExistingRecipient in ipairs(self.tRecipients) do
		if tRecipient:GetFullname() == tExistingRecipient:GetFullname() then
			tRecipient:Destroy()
			table.remove(self.tRecipients, nIndex)
			break
		end
	end
end

function RecipientCollection:ArrangeAndFilter()
	local nHF = 1.0 / self.nMaxRows -- Horizontal factor
	local nWF = 1.0 / self.nColumns -- Vertical factor
	local nSC = 0                   -- Skip count
	
	for nIndex, tRecipient in pairs(self.tRecipients) do
		nIndex = nIndex - 1 - nSC
		
		if tRecipient:GetUnit() == nil or Utils:HaveArmorType(tRecipient:GetUnit():GetClassId(), self.strCurrentFilter) then
			local nRow = math.floor(nIndex / self.nColumns)
			local nCol = nIndex % self.nColumns
			
			tRecipient:SetAnchor({
				tPoints  = {nWF * nCol, nHF * nRow, nWF * (nCol+1), nHF * (nRow+1)},
			    tOffsets = {0, 0, -1, -1}
			})
			tRecipient:Show()
		else 
			tRecipient:Hide()
			nSC = nSC + 1
		end
	end
end

function RecipientCollection:SetCallback(tCallback)
	self.tCallback = tCallback
end

function RecipientCollection:GetSelectedRecipient()
	return self.tSelection
end

function RecipientCollection:GetRaider(strUnitName)
	for _, tRecipient in pairs(self.tRecipients) do
		if tRecipient:IsNamed(strUnitName) then
			return tRecipient
		end
	end
	
	return nil
end

function RecipientCollection:UnselectCurrent()
	if self.tSelection ~= nil then
		self.tSelection:Unselect()
	end
end

----------------------------------------------------------------------------------
-- Events
----------------------------------------------------------------------------------
function RecipientCollection:OnPlayerInspectionStarted(tUnit, tRecipient)
	if self.tInspector ~= nil then
		self.tInspector:StopInspection()
	end

	self.tInspector = tRecipient
	Utils:FireEvent(self.tCallback, "OnPlayerInspectionStarted", tUnit, tRecipient)
end

function RecipientCollection:OnPlayerInspectionStoped()
	if self.tInspector ~= nil then
		self.tInspector:StopInspection()
	end
	
	self.tInspector = nil
	Utils:FireEvent(self.tCallback, "OnPlayerInspectionStoped")
end

function RecipientCollection:OnRecipientSelected(tRecipient)
	if self.tSelection ~= nil then 
		local tToUnselect = self.tSelection -- Break the recursion
		self.tSelection = nil
		tToUnselect:Unselect()
	end
	
	local tOldRecipient = self.tSelection
	self.tSelection = tRecipient
	
	Utils:FireEvent(self.tCallback, "OnRecipientSelectionChanged", tOldRecipient, tRecipient)
end

function RecipientCollection:OnRecipientUnselected(tRecipient)
	local tOldRecipient = self.tSelection
	self.tSelection = nil
	
	Utils:FireEvent(self.tCallback, "OnRecipientSelectionChanged", tOldRecipient, nil)
end

function RecipientCollection:OnFilterChange(wndHandler, wndControl, eMouseButton)
	if wndControl:IsChecked() then
		local strButtonName = wndControl:GetName()
		local _, strButtonId = Utils:Split(strButtonName, "_")
		local strFilter = nil 
		
		if strButtonId == "AllButton" then
			strFilter = "A"
		elseif strButtonId == "HButton" then
			strFilter = "H"
		elseif strButtonId == "MButton" then
			strFilter = "M"
		elseif strButtonId == "LButton" then
			strFilter = "L"
		else 
			strFilter = "A"
		end
		
		if strFilter ~= self.strCurrentFilter then
			self.strCurrentFilter = strFilter
			Utils:Log("Filter settings changed to: " .. strFilter)
			self:ApplyFilter()
			self:UnselectCurrent()
		end
	end
end

ClassFactory:RegisterClass("RecipientCollection", RecipientCollection)