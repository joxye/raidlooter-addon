--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
-- ChatParser 
-- Continuously minitors the party chat and generate events from text written.
-- This class is the core of this addon and is what drives the distribution of 
-- loot.
--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--

----------------------------------------------------------------------------------
-- Initialization
----------------------------------------------------------------------------------
function ChatParser:New()
	local meta = {
		bIsParsing    = false,
		bIsForced     = true,
		bIsRolling    = false,
		tProcessTimer = nil,
		tAuthSenders  = {},
		tCombatants   = {},
		tCallbacks    = {},
		tOffspecWords = {"os","OS","Os","oS","off","Off","OFF","of","Offspec","offspec","ofspec","off-spec","Off-Spec","Off-spec","Off spec","off spec","Off Spec","offspek"},
		tCostumeWords = {"Costume","costume","costum","Costum","cost","cos"},
		tMessageQueue = Queue:new(),
		nCombatants   = 0,
		eParseChannel = Debug.bIsTest and ChatSystemLib.ChatChannel_Say or ChatSystemLib.ChatChannel_Party,
	}
	
	local self = ClassFactory:SetupInstance(meta, ChatParser)
	self.tProcessTimer = self:CreateProcessTimer()
	
	return self
end

function ChatParser:Destroy()
end

function ChatParser:CreateProcessTimer()
	local tProcessTimer = ApolloTimer.Create(5, true, "OnSlowProcessTimer", self)
	tProcessTimer:Stop()
	
	return tProcessTimer
end

----------------------------------------------------------------------------------
-- Functions
----------------------------------------------------------------------------------
function ChatParser:IsSenderAuthorized(strSender)
	return self.tAuthSenders[strSender] ~= nil or Utils:TableLength(self.tAuthSenders) == 0 or Debug.bIsTest
end

function ChatParser:Start()
	if not self.bIsParsing then		
		Utils:Log("ChatParser Starting")
		Apollo.RegisterEventHandler("ChatMessage", "OnChatMessage", self)
		Apollo.RegisterEventHandler("VarChange_FrameCount", "OnChatParseTimer", self)
		
		self.tProcessTimer:Start()
		self:OnSlowProcessTimer()
		self.bIsParsing = true
		self:Notify("OnParsingStarted_ChatParser")
	end
end

function ChatParser:ForceStop()
	Utils:Log("ChatParser ForceStoping")
	Apollo.RemoveEventHandler("ChatMessage", self)
	Apollo.RemoveEventHandler("VarChange_FrameCount", self)
	
	self.bIsForced = true
	self.bIsParsing = false
	self.tProcessTimer:Stop()
	self:Notify("OnParsingStoped_ChatParser")
end

-- Should Respond to:
--     OnRoll(strUnitName, nScore)
--     OnBeginRoll(strUnitName, tItem)
--     OnEndRoll(strUnitName)
--     OnRollHint(strUnitName, tHint)
--     OnParsingStoped_ChatParser()
--     OnParsingStarted_ChatParser()
function ChatParser:RegisterCallback(tCallback)
	table.insert(self.tCallbacks, tCallback)
end

function ChatParser:UnregisterCallback(tCallback)
	for nIndex, tStoredCallback in ipairs(self.tCallbacks) do
		if tStoredCallback == tCallback then
			table.remove(self.tCallbacks, nIndex)
		end
	end
end

function ChatParser:IsParsing()
	return self.bIsParsing
end

----------------------------------------------------------------------------------
-- Event handlers
----------------------------------------------------------------------------------
function ChatParser:OnChatParseTimer()
	local tMessagePost = nil
	while not self.tMessageQueue:Empty() do
		tMessagePost = self.tMessageQueue:Pop()	
		
		if (tMessagePost.eChannelType == ChatSystemLib.ChatChannel_System and self.bIsRolling) then
			self:OnChatParseTimer_SystemParse(tMessagePost)
		elseif tMessagePost.eChannelType == self.eParseChannel then	
			self:OnChatParseTimer_PartyMessage(tMessagePost)
		end
	end 
end

function ChatParser:OnChatParseTimer_SystemParse(tMessagePost)
	local tMessages = tMessagePost.tMessage.arMessageSegments

	if tMessages[1] ~= nil and tMessages[1].strText ~= nil then
		self:OnSystemMessage(tMessages[1].strText)
	end
end

function ChatParser:OnChatParseTimer_PartyMessage(tMessagePost)
	local tMessages = tMessagePost.tMessage.arMessageSegments
	local tItem     = nil
	
	if tMessages[2] ~= nil then
		tItem = tMessages[2].uItem
	end
	
	if tMessages[1] ~= nil and tMessages[1].strText ~= nil then
		self:OnPartyMessage(tMessages[1].strText, tItem, tMessagePost.tMessage.strSender)
	end
end

-- Beware --
-- This is a workaround for being able to authorize the party members 
-- as they post in the party chat. As we will only have the unit-name 
-- of out-of-range units, another client can't verify if the poster have 
-- permissions.
--
-- As a side-effect the player must have been in-range of the leader and/or
-- the assistant posting the roll message for anything to happen.
function ChatParser:OnSlowProcessTimer()
	-- WONT FIX: This can be improved by stopping when all members have 
	--           been in range at least once and start listening for 
	--           member flag changed events.
	--
	-- This will be called each 5 seconds.
	local nMembers = GroupLib.GetMemberCount()
	for nIndex = 1, nMembers do
		local tUnitMember = GroupLib.GetUnitForGroupMember(nIndex)
		local tMember     = GroupLib.GetGroupMember(nIndex)
		
		if tUnitMember ~= nil and tUnitMember:IsValid() and tMember ~= nil then
			if tMember.bRaidAssistant or tMember.bIsLeader then
				self.tAuthSenders[tUnitMember:GetName()] = true
			else 
				self.tAuthSenders[tUnitMember:GetName()] = nil
			end
		end
	end
end

function ChatParser:OnChatMessage(tChannel, tMessage)
	-- Warning --
	-- Trying to store tMessage or any part of it will not work. I am guessing 
	-- the real object somehow is destoryed or it might be an async issue ... 
	-- However, pushing it to a Queue like in the ChatLog Addon seam to circumvent the 
	-- problem. 
	--
	-- So you can't put it in self here or send it to Print etc (because Wildstar Client 
	-- will crash), at least that was what happend pre F2P.
	local eChannelType  = tChannel:GetType()
	local bAllowThrough = nil
	
	bAllowThrough = (eChannelType == ChatSystemLib.ChatChannel_System) and self.bIsRolling
	bAllowThrough = bAllowThrough or (eChannelType == self.eParseChannel)
	
	if bAllowThrough then
		self.tMessageQueue:Push({
			tMessage          = tMessage,
			eChannelType      = tChannel:GetType(),
			strChannelName    = tChannel:GetName(),
			strChannelCommand = tChannel:GetCommand(),
			idChannel         = tChannel:GetUniqueId()
		})
	end
end

----------------------------------------------------------------------------------
-- Private functions
----------------------------------------------------------------------------------
function ChatParser:IsRaidMember(tUnit)
	if Debug.bIsTest then
		return tUnit:IsThePlayer()
	else
		return tUnit:IsACharacter() and tUnit:IsInYourGroup()
	end
end

function ChatParser:OnSystemMessage(strText)
	local strForename, strLastname, strC1, strValue, strC2 = Utils:Split(strText, " ")
	
	if self:IsControlValuesMet(strC1, strC2) then
		local nScore = tonumber(strValue)
		
		if self:IsRollInRange(nScore) then     
			self:Notify("OnRoll_ChatParser", strForename.." "..strLastname, nScore)
		end
	end	
end

function ChatParser:OnPartyMessage(strText, tItem, strSender)
	if self.bIsRolling then
		if self:IsAnOffspecHint(strText) then
			self:SendRollHintSignal(true, strSender)	
		elseif self:IsAnCostumeHint(strText) then
			self:SendRollHintSignal(false, strSender)	
		elseif self:IsSenderAuthorized(strSender) then
			if self:IsNoRolls(strText) then
				self:Notify("OnEndRoll_ChatParser", strSender)
			else 
				self:ParsePotentailRolls(strText, strSender)
			end
			
			self.bIsRolling = false
		end
	elseif self:IsARollStart(strText, tItem, strSender) then
		self:Notify("OnBeginRoll_ChatParser", strSender, tItem)
		self.bIsRolling = true
	end
end

function ChatParser:ParsePotentailRolls(strText, strSender)
	local strTwoFirstWords = self:GetFirstTwoWords(strText)
			
	if self:IsOneWinner(strTwoFirstWords) or self:IsATie(strTwoFirstWords) then
		self:Notify("OnEndRoll_ChatParser", strSender)
	end
end

function ChatParser:GetFirstTwoWords(strText)
	local strWord1, strWord2 = Utils:Split(strText, " ")
		
	if strWord1 ~= nil and strWord2 ~= nil then
		return strWord1.." "..strWord2
	end
	
	return nil
end

function ChatParser:SendRollHintSignal(bIsOffspec, strSender)
	local tHint = {
		bIsOffspec = bIsOffspec, 
		bIsCostume = not bIsOffspec
	}

	self:Notify("OnRollHint_ChatParser", strSender, tHint)
end

function ChatParser:MatchesAnyOf(tAvailable, strWord)
	for nIndex, strAvailableWord in pairs(tAvailable) do
		if strWord == strAvailableWord then
			return true
		end
	end
	
	return false
end

function ChatParser:IsControlValuesMet(strC1, strC2)
	return strC1 == "rolls" and strC2 == "(1-100)"
end

function ChatParser:IsRollInRange(nScore)
	return nScore ~= nil and 1 <= nScore and nScore <= 100
end

function ChatParser:IsARollStart(strText, tItem, strSender)
	return self:IsSenderAuthorized(strSender) and (strText == "Roll for: " or strText == "Reroll for: ") and tItem ~= nil
end

function ChatParser:IsNoRolls(strText)
	return self:IsTextMatching(strText, "No rolls.")
end

function ChatParser:IsOneWinner(strText)
	return self:IsTextMatching(strText, "Best roll:")
end

function ChatParser:IsATie(strText)
	return self:IsTextMatching(strText, "Tie between:")
end

function ChatParser:IsTextMatching(strText, strActualText)
	return strText ~= nil and strText == strActualText 
end

function ChatParser:IsAnOffspecHint(strText)
	return self:MatchesAnyOf(self.tOffspecWords, strText)
end

function ChatParser:IsAnCostumeHint(strText)
	return self:MatchesAnyOf(self.tCostumeWords, strText)
end

function ChatParser:Notify(strEventName, ...)
	Utils:Log("Notify => " .. strEventName)
	
	for _, tCallback in ipairs(self.tCallbacks) do
		Utils:FireEvent(tCallback, strEventName, ...)
	end
end

-- Load
ChatParser = RaidLooter:LoadSingleton("ChatParser", ChatParser )