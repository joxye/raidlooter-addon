--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
-- Settings
-- Abstracation for reading/writing settings. 
--
-- Put all defaults in the defaults object in the constructor. 
--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--

----------------------------------------------------------------------------------
-- Initialization
----------------------------------------------------------------------------------
function Settings:New()
	local meta = {
		bIsLoaded = false,
		tDefaults = {
			strVersion = CURRENT_VERSION,
			tGeneral = {
				bUseRollPopups = true,
				bOpenOnLoot    = true,
				bUseOverlay    = true,
				bUseSound      = false,
				bUseRollSound  = false,
				bUseLootSound  = false,
			},
			tLeader  = {
				bUseAutoSelection = true,
				bAllowMultiSelection = true, 
				bDetailedInspect = false, 
			},
			tWindowLocations = {
				wndOverlayLoc = {},
				wndLootWindowLoc = {},	
				wndManageWindowLoc = {}
			}
		},
		tSettings = {},
		tNewSettings = nil,
		tCallbacks = {}
	}

	return ClassFactory:SetupInstance(meta, Settings)
end

function Settings:Destroy()	
end

----------------------------------------------------------------------------------
-- Functions
----------------------------------------------------------------------------------
function Settings:Save(eLevel)
	if eLevel ~= GameLib.CodeEnumAddonSaveLevel.Character then
        return nil
    end
	return Utils:Deepcopy(self.tSettings)
end

function Settings:Load(eLevel, tData)
	if eLevel ~= GameLib.CodeEnumAddonSaveLevel.Character then
        return nil
    end

	-- Start with default settings.
	self.tSettings = Utils:Deepcopy(self.tDefaults)
	
	if tData ~= nil then
		-- Make a copy of saved settings only if they exists.
		self.tNewSettings = Utils:Deepcopy(tData)
		self.bIsNewVersion = CURRENT_VERSION ~= self.tNewSettings["strVersion"]
		
		-- We call RestoreSettings to make sure we only restore currently known
		-- settings. That is we should only care about settings currently in 
		-- the defaults objects and discared others.
		self:RestoreSettings(self.tSettings, self.tNewSettings)
	end
end

function Settings:ReadSetting(strKeySequence, default)
	local strKeys = Utils:Explode(".", strKeySequence)
	local nNumKeys = #strKeys
	local tSettings = self.tSettings
	
	for nIndex, strKey in ipairs(strKeys) do
		if nIndex == nNumKeys then
			local setting = tSettings[strKey]
			if setting == nil then
				return default
			else 
				return setting
			end
		else 
			tSettings = tSettings[strKey]
			if tSettings == nil then
				return default
			end
		end 
	end

	return default
end

function Settings:WriteSetting(strKeySequence, value)
	local strKey, strRest = self:FirstKeyAndRest(strKeySequence)
	
	if self.tSettings[strKey] ~= nil then
		if strRest == nil then
			self.tSettings[strKey] = value
		else 
			local tValue = self:WriteSettingHelper(self.tSettings[strKey], strRest, value)
		
			if tValue ~= nil then
				self.tSettings[strKey] = tValue
			else 
				Utils:Log("Unable to write to setting: " .. strKeySequence)
			end 		
		end 
	else
		Utils:Log("Unable to write to setting: " .. strKeySequence)
	end
end

function Settings:GetTable()
	return self.tSettings
end

function Settings:GetDefaultTable()
	return self.tDefaults
end

function Settings:GetNewTable()
	return self.tNewSettings
end

----------------------------------------------------------------------------------
-- Private functions
----------------------------------------------------------------------------------
function Settings:RestoreSettings(tCurrentSettings, tNewSettings, bIsWindowLocation)
	for strKey, tCurrentValue in pairs(tCurrentSettings) do
		local tNewValue = tNewSettings[strKey]
		
		if tNewValue ~= nil then
			if not bIsWindowLocation and type(tNewValue) == 'table' then
				self:RestoreSettings(tCurrentValue, tNewValue, strKey == "tWindowLocations")
			else 
				if not (bIsWindowLocation and self.bIsNewVersion) then
					tCurrentSettings[strKey] = tNewValue
				end
			end
		end
	end
end

function Settings:FirstKeyAndRest(strKeySequence)
	local arrKeys = Utils:Explode(".", strKeySequence)
	local strFirstKey = nil
	local strRestKeys = nil
	
	for nIndex, strKey in ipairs(arrKeys) do
		if nIndex == 1 then
			strFirstKey = strKey
		elseif nIndex == 2 then
			strRestKeys = strKey
		else 
			strRestKeys = strRestKeys.."."..strKey
		end
	end
	
	return strFirstKey, strRestKeys
end

function Settings:WriteSettingHelper(tSettings, strKeySequence, value)
	local strKey, strRest = self:FirstKeyAndRest(strKeySequence)
	
	if tSettings[strKey] ~= nil then
		if strRest == nil then
			tSettings[strKey] = value
			return tSettings
		else
			tSettings[strKey] = WriteSettingHelper(tSettings[strKey], strRest, value)
		end
	end
	
	return nil
end

-- Load
Settings = RaidLooter:LoadSingleton("Settings", Settings)
