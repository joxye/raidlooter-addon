--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
-- Distribution View 
-- Represents that distribution view/tab of the addon. This is the tab the raid 
-- leader/assitant will use to distribute loot to other raiders.  
--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
local DistributionView = {}
local SelectionEnum = {
	None           = 1,
	ItemOnly       = 2,
	ItemAndPlayer  = 3,
	RollingForItem = 4,
	MultipleItems  = 5
}

----------------------------------------------------------------------------------
-- Initialization
----------------------------------------------------------------------------------
function DistributionView:New(bIsAuthorized, bIsInRaid)
	local meta = {
		wndMain         = nil,
		wndFrame        = nil,
		wndLoot         = nil,
		wndRecp         = nil,
		wndRollerList   = nil,
		wndWarn         = nil,
		wndItemLocked   = nil,
		wndItemStop     = nil,
		wndElapsedTime  = nil,
		btnClearRolls   = nil,
		btnReset        = nil,
		btnTab          = nil,
		btnAssign       = nil,
		btnBeginRoll    = nil,
		btnEndRoll      = nil,
		btnRandomize    = nil, 
		btnReroll       = nil,
		nElapsedTime    = 0,
		tElapsedTimer   = nil,
		tLootSet        = nil,
		tRecipientSet   = nil,
		tRollSet        = nil,
		bIsAuthorized    = bIsAuthorized,
		bIsInRaid         = bIsInRaid,
		bIsFullViewLoaded = false,
		eState            = SelectionEnum.None,
		tInspect = {
			tUnit = nil,
			tItems = nil,
		},
		tFilter = {
			wnd       = nil,
			btnAll    = nil,
			btnHeavy  = nil,
			btnMedium = nil,
			btnLight  = nil
		},	
		tAuth = {
			strRequestText = "Waiting for the leader",
			nTimeoutTime   = 3,
			nSpamTime      = 5,
			nTickCount     = 15,
			tRequestTimer  = nil,
			tSpamTimer     = nil,
			btnRequest     = nil,
			wndRequest     = nil,
			wndDenied      = nil,
			wndTimeout     = nil
		},
		tSelectedRaider = { 
			tRecipient = nil,
			bIsLocked  = false 
		},
		tSelectedLootItem = {
			wndIcon     = nil, 
			wndName     = nil,
			wndInfo     = nil, 
			tItem       = nil,
			bIsLocked   = false,
			bIsMultiple = false
		},
		fSavedOnInspect = nil,
	}
	
	local self = ClassFactory:SetupInstance(meta, DistributionView)
	self.tElapsedTimer = ApolloTimer.Create(1, true, "OnOneSecondElapsed", self)
	self.tElapsedTimer:Stop()
	return self
end

function DistributionView:Destroy()
	if self.tLootSet ~= nil then
		self.tLootSet:Destroy()
	end 
	
	if self.tRecipientSet ~= nil then
		self.tRecipientSet:Destroy()
	end
	
	if self.wndRollerList ~= nil then
		self.wndRollerList:Destroy()
	end
end

function DistributionView:Load(wndParent)
	if self.wndFrame ~= nil then 
		self.wndFrame:Close()
	end

	self.wndMain = wndParent
	self.btnTab  = self.wndMain:FindChild("DistributionPageButton")
	self:LoadFullView()
	self.btnTab:AttachWindow(self.wndFrame)
	self.btnTab:SetCheck(true)
	self.btnTab:Enable(true)
end

function DistributionView:LoadFullViewFormAuthView()
	if self.wndFrame ~= nil then
		self.wndFrame:Show(false)
	end

	self:LoadFullView()
	self.btnTab:AttachWindow(self.wndFrame)
	self.btnTab:SetCheck(true)
	self.btnTab:Enable(true)
	self.wndFrame:Show(true)
end

function DistributionView:LoadAuthViewFromFullView()
	if self.wndFrame ~= nil then
		self.wndFrame:Show(false)
	end
	
	self:LoadAuthorizationView()
	self.btnTab:AttachWindow(self.wndFrame)
	self.btnTab:SetCheck(true)
	self.btnTab:Enable(true)
	self.wndFrame:Show(true)
end

function DistributionView:LoadAuthorizationView()
	self.wndFrame         = self.wndMain:FindChild("NotAuthorizedPage") 
	self.tAuth.btnRequest = self.wndMain:FindChild("ReqAssistPermissionsBtn")             -- Auth
	self.tAuth.wndRequest = self.wndMain:FindChild("WaitMessage")
	self.tAuth.wndDenied  = self.wndMain:FindChild("AuthMessage")
	self.tAuth.wndTimeout = self.wndMain:FindChild("TimeoutMessage")
	self.bIsFullViewLoaded = false
end

function DistributionView:LoadFullView()
	self.wndFrame                  = self.wndMain:FindChild("DistributionPage")           -- Windows
	self.wndLoot                   = self.wndMain:FindChild("AvailableItemContainer")
	self.wndRecp                   = self.wndMain:FindChild("RecipientsList")
	self.wndRollerList             = self.wndMain:FindChild("RollTopList-Dist")
	self.wndWarn                   = self.wndMain:FindChild("NoItemSelectedWarning")
	self.wndItemStop               = self.wndMain:FindChild("ItemStop")
	self.wndItemLocked             = self.wndMain:FindChild("ItemLocked")
	self.wndElapsedTime            = self.wndMain:FindChild("Elapsed")
	self.btnReset                  = self.wndMain:FindChild("ResetButton")
	self.btnAssign                 = self.wndMain:FindChild("AssignButton")               -- Buttons
	self.btnBeginRoll              = self.wndMain:FindChild("BeginRollButton")
	self.btnEndRoll                = self.wndMain:FindChild("EndRollButton")
	self.btnRandomize              = self.wndMain:FindChild("RandomizeButton")
	self.btnReroll                 = self.wndMain:FindChild("RerollButton")
	self.btnClearRolls             = self.wndMain:FindChild("ClearRollsButton")
	self.tSelectedLootItem.wndIcon = self.wndMain:FindChild("SelectedItemIcon")           -- The selected item
	self.tSelectedLootItem.wndName = self.wndMain:FindChild("SelectedItemName")
	self.tSelectedLootItem.wndInfo = self.wndMain:FindChild("SelectedItemInfo")
	self.tFilter.wnd               = self.wndMain:FindChild("RecipientFilter")            -- Filters
	self.tFilter.btnAll            = self.wndMain:FindChild("Filter_AllButton")
	self.tFilter.btnHeavy          = self.wndMain:FindChild("Filter_HButton")
	self.tFilter.btnMedium         = self.wndMain:FindChild("Filter_MButton")
	self.tFilter.btnLight          = self.wndMain:FindChild("Filter_LButton")
	
	-- Set the initial state for some controls.
	self.tSelectedLootItem.wndInfo:Show(false, true)
	self.btnAssign:Show(false, true)
	self.btnBeginRoll:Show(false, true)
	self.btnEndRoll:Show(false, true)
	self.btnRandomize:Show(false, true)
	self.btnReroll:Show(false, true)
	self.tFilter.btnAll:SetCheck(true)
	self.wndItemStop:Show(false, true)
	self.wndItemLocked:Show(false, true)
	
	if not Utils:IsLeader() then
		self.btnAssign:Enable(false)
		self.btnRandomize:Enable(false)
	end
	
	self:DisplayNoItemSelected()
	
	-- Collection of raiders, loot and rollers
	self.tLootSet      = ClassFactory:CreateInstance("LootItemCollection", true, true, self.wndLoot)
	self.tRecipientSet = ClassFactory:CreateInstance("RecipientCollection", self.wndRecp)
	self.tRollSet      = ClassFactory:CreateInstance("RollerCollection", true, self.wndRollerList)

	self.tLootSet:SetCompareFunction(function(tItem) 
		return self:GetItemForComparision(tItem)
	end)
	self.tLootSet:SetCallback(self)
	self.tRecipientSet:SetCallback(self)
	self.tRollSet:SetCallback(self)
	
	self.bIsFullViewLoaded = true
end

----------------------------------------------------------------------------------
-- Functions
----------------------------------------------------------------------------------
function DistributionView:IsFullViewLoaded()
	return self.bIsFullViewLoaded
end

function DistributionView:GetItemForComparision(tItem)
	if self.tInspect.tUnit ~= nil and self.tInspect.tUnit:IsValid() then
		local eItemSlot = tItem:GetSlot()
		
		for nIndex, tEquippedItem in pairs(self.tInspect.tItems) do
			if tEquippedItem:GetSlot() == eItemSlot then
				return tEquippedItem
			end
		end

		-- Nothing to compare against because is is not an equippable item.
	end
	return nil
end

----------------------------------------------------------------------------------
-- Event handlers
----------------------------------------------------------------------------------
function DistributionView:HandleOnLootAssigned(tItem, strLooter)
	local strFormat  = Apollo.GetString("CRB_MasterLoot_AssignMsg")
	local strMessage = String_GetWeaselString(strFormat, tItem:GetName(), strLooter);
	
	Event_FireGenericEvent("GenericEvent_LootChannelMessage", strMessage)
end

function DistributionView:HandleOnLootUpdate(tLoot)
	if self.tLootSet ~= nil then
		self.tLootSet:UpdateLoot(tLoot)
	end
	
	self:UpdateRecipients()	
end

function DistributionView:HandleOnLootItemSelectionChanged(tSelectedItems)
	local nNumSelected        = #tSelectedItems
	local bIsNoneSelected     = nNumSelected == 0
	local bIsMultipleSelected = nNumSelected > 1
	
	if bIsNoneSelected then
		self:Reset()
	elseif bIsMultipleSelected then
		self:SelectMultipleItems()
	else
		self:SelectSingleItem(tSelectedItems[1])
	end	
end

function DistributionView:HandleOnRecipientSelectionChanged(tOldRecipient, tNewRecipient)
	-- Update selected raider.
	self.tSelectedRaider.tRecipient = tNewRecipient
	
	if self.eState ~= SelectionEnum.RollingForItem then
		if tNewRecipient == nil then
			self.eState = SelectionEnum.ItemOnly
		elseif self.eState == SelectionEnum.ItemOnly then
			self.eState = SelectionEnum.ItemAndPlayer
		end
		
		self:UpdateVisibleButtons()
	end
end

function DistributionView:HandleOnAssignLoot()
	Utils:PlaySound(236)
	self:AssignLootHelper(false)
end

function DistributionView:HandleOnBeginRoll(bIsReroll)
	Utils:PlaySound(173)
	
	-- This is called when the current 'player" invokes "Begin Roll" button. 
    local bIsRolling = true
	local strTieString = self.tRollSet:FormatTopTie()
	
	self:SyncToState(bIsRolling)
	self:UpdateVisibleButtons()
	self:SendBeginRollToChat(bIsReroll, strTieString)
end

function DistributionView:HandleOnEndRoll()
	Utils:PlaySound(173)
	-- This is called when the current 'player" invokes "End Roll" button. 
	local bIsRolling = false
	
	self:SyncToState(bIsRolling)
	self:SendEndRollToChat()
	self:UpdateVisibleButtons()
end

function DistributionView:HandleOnRandomizeLoot()
	Utils:PlaySound(173)
	if self.tSelectedLootItem.bIsMultiple then
		local tSelectedItems = self.tLootSet:GetSelectedItems()
		for _, tItem in ipairs(tSelectedItems) do
			self.tSelectedLootItem.tItem = tItem
			self:AssignLootHelper(true)
		end
	else 
		self:AssignLootHelper(true)
	end
end

function DistributionView:HandleOnReset()
	Utils:PlaySound(229)
	self:Reset()
end

function DistributionView:HandleOnReroll()
	self:HandleOnBeginRoll(true)
end

function DistributionView:HandleOnFilterToggle()
	local wnd = self.tFilter.wnd
	if wnd ~= nil then
		wnd:Show(not wnd:IsShown(), false)
	end
end

function DistributionView:HandleOnCurrentRecipientDeselect()
	if self.bIsAuthorized then
		self.tRecipientSet:UnselectCurrent()
		self.tSelectedRaider.tRecipient = nil
		self.eState = SelectionEnum.ItemOnly
		self:UpdateVisibleButtons()
	end
end

function DistributionView:HandleOnCurrentItemDeselect()
	if self.bIsAuthorized then
		if not self.tSelectedLootItem.bIsLocked then
			self:Reset()
		end
	end
end

function DistributionView:HandleOnRollerEvent_Tied(tRaider, nScore)
	if self.bIsAuthorized then
		if self.btnReroll ~= nil then
			self.btnReroll:Enable(true)
		end
	end
end

function DistributionView:HandleOnRollerEvent_NewLeader(tRaider, nScore)	
	if self.bIsAuthorized then
		if self.btnReroll ~= nil then
			self.btnReroll:Enable(false)
		end
	end
end

function DistributionView:HandleOnClearLatestRolles()
	if self.bIsAuthorized then
		if self.tRollSet ~= nil then
			self.tRollSet:Clear()
		end
	end
end

function DistributionView:HandleOnInspect(tUnit, tItems)
	if self.bIsAuthorized then
		self.tInspect.tUnit  = tUnit
		self.tInspect.tItems = tItems
	end
end

function DistributionView:HandleOnPlayerInspectionStarted(tUnit, tRecipient)
	if self.bIsAuthorized then
		if tUnit ~= nil and tUnit:IsValid() then
			if not Settings:ReadSetting("tLeader.bDetailedInspect") then
				local tDependentAddon = Apollo.GetAddon("Character")
				if tDependentAddon ~= nil and tDependentAddon["OnInspect"] ~= nil then
					do 
						self.fSavedOnInspect = tDependentAddon.OnInspect
						tDependentAddon.OnInspect = function(self, unitTarget, arItems)
							-- Do nothing ...
						end
						self.tOnReinsertOnInspectTimer = ApolloTimer.Create(1, false, "OnReinsertOnInspect", self)
					end
				end
			end
		
			tUnit:Inspect() -- This will trigger the "Inspect"-event
		end
	end
end

function DistributionView:OnReinsertOnInspect()
	local tDependentAddon = Apollo.GetAddon("Character")
	if tDependentAddon ~= nil then
		local fTarget = tDependentAddon["OnInspect"]
		
		if fTarget ~= nil then
			tDependentAddon.OnInspect = self.fSavedOnInspect
		end
	end
end

function DistributionView:HandleOnPlayerInspectionStoped()
	if self.bIsAuthorized then
		self.tInspect = {}
	end
end

function DistributionView:HandleOnRoll_ChatParser(strUnitName, nScore)
	-- This event is generated by the chat parsing.
	if self.bIsAuthorized then
		if self.eState == SelectionEnum.RollingForItem then
			self.tRollSet:AddRoller(strUnitName, nScore)
		end
	end
end

function DistributionView:HandleOnBeginRoll_ChatParser(strUnitName, tItem)
	-- This event is generated by the chat parsing.
	-- Make sure to sync accordingly.
	if self.bIsAuthorized then
		local bIsRolling = true
		
		self.tSelectedLootItem.tItem = self.tLootSet:GetItem(tItem)
		self:SyncToState(bIsRolling)
	end
end

function DistributionView:HandleOnEndRoll_ChatParser(strUnitName)
	-- This event is generated by the chat parsing.
	-- Make sure to sync accordingly.
	if self.bIsAuthorized then
		local bIsRolling = false
		self:SyncToState(bIsRolling)
	end
end

function DistributionView:HandleOnRollHint_ChatParser(strUnitName, tHint)
	-- This event is generated by the chat parsing.
	if self.bIsAuthorized then
		self.tRollSet:UpdateRollHint(strUnitName, tHint)
	end
end

function DistributionView:HandleOnParsingStarted_ChatParser()
	-- This event is generated by the chat parsing.
	if self.bIsAuthorized then
		self:UpdateVisibleButtons()
	end
end

function DistributionView:HandleOnParsingStoped_ChatParser()
	-- This event is generated by the chat parsing.
	if self.bIsAuthorized then
		self:UpdateVisibleButtonsHelper(false, false, false, false, false)
	end
end

----------------------------------------------------------------------------------
-- Events
----------------------------------------------------------------------------------
function DistributionView:OnLootAssigned(tItem, strLooter)
    Utils:InvokeEventHandler(self, "DistributionView", "OnLootAssigned", tItem, strLooter)
end

function DistributionView:OnLootUpdate(tLoot)
	Utils:InvokeEventHandler(self, "DistributionView", "OnLootUpdate", tLoot)
end

function DistributionView:OnLootItemSelectionChanged(tSelectedItems)
	Utils:InvokeEventHandler(self, "DistributionView", "OnLootItemSelectionChanged", tSelectedItems)
end

function DistributionView:OnRecipientSelectionChanged(tOldRecipient, tNewRecipient)
	Utils:InvokeEventHandler(self, "DistributionView", "OnRecipientSelectionChanged", tOldRecipient, tNewRecipient)
end

function DistributionView:OnAssignLoot(wndHandler, wndControl, eMouseButton)
	Utils:InvokeEventHandler(self, "DistributionView", "OnAssignLoot")
end

function DistributionView:OnBeginRoll(wndHandler, wndControl, eMouseButton)
	Utils:InvokeEventHandler(self, "DistributionView", "OnBeginRoll")
end

function DistributionView:OnEndRoll(wndHandler, wndControl, eMouseButton)
	Utils:InvokeEventHandler(self, "DistributionView", "OnEndRoll")
end

function DistributionView:OnRequestAssistPremissions(wndHandler, wndControl, eMouseButton)
	Utils:InvokeEventHandler(self, "DistributionView", "OnRequestAssistPremissions")
end

function DistributionView:OnAssistPermissionsGranted()
	Utils:InvokeEventHandler(self, "DistributionView", "OnAssistPermissionsGranted")
end

function DistributionView:OnAssistPermissionDenied()
	Utils:InvokeEventHandler(self, "DistributionView", "OnAssistPermissionDenied")
end

function DistributionView:OnRequestAssistPremissions_Tick()
	Utils:InvokeEventHandler(self, "DistributionView", "OnRequestAssistPremissions_Tick")
end

function DistributionView:OnRequestAssistPremissions_Timeout()
	Utils:InvokeEventHandler(self, "DistributionView", "OnRequestAssistPremissions_Timeout")
end

function DistributionView:OnRequestAssistPremissions_SpamProtection()
	Utils:InvokeEventHandler(self, "DistributionView", "OnRequestAssistPremissions_SpamProtection")
end

function DistributionView:OnRandomizeLoot(wndHandler, wndControl, eMouseButton)
	Utils:InvokeEventHandler(self, "DistributionView", "OnRandomizeLoot")
end

function DistributionView:OnReset(wndHandler, wndControl, eMouseButton)
	Utils:InvokeEventHandler(self, "DistributionView", "OnReset")
end

function DistributionView:OnReroll(wndHandler, wndControl, eMouseButton)
	Utils:InvokeEventHandler(self, "DistributionView", "OnReroll")
end

function DistributionView:OnFilterToggle(wndHandler, wndControl, eMouseButton)
	Utils:InvokeEventHandler(self, "DistributionView", "OnFilterToggle")
end

function DistributionView:OnCurrentRecipientDeselect(wndHandler, wndControl, eMouseButton)
	Utils:InvokeEventHandler(self, "DistributionView", "OnCurrentRecipientDeselect")
end

function DistributionView:OnCurrentItemDeselect(wndHandler, wndControl, eMouseButton)
	Utils:InvokeEventHandler(self, "DistributionView", "OnCurrentItemDeselect")
end

function DistributionView:OnInspect(tUnit, tItems)
	Utils:InvokeEventHandler(self, "DistributionView", "OnInspect", tUnit, tItems)
end

function DistributionView:OnRollerEvent_Tied(tRaider, nScore)
	Utils:InvokeEventHandler(self, "DistributionView", "OnRollerEvent_Tied", tRaider, nScore)
end

function DistributionView:OnRollerEvent_NewLeader(tRaider, nScore)
	Utils:InvokeEventHandler(self, "DistributionView", "OnRollerEvent_NewLeader", tRaider, nScore)
end

function DistributionView:OnClearLatestRolles(wndHandler, wndControl, eMouseButton)
	Utils:InvokeEventHandler(self, "DistributionView", "OnClearLatestRolles")
end

function DistributionView:OnRoll_ChatParser(strUnitName, nScore)
	Utils:InvokeEventHandler(self, "DistributionView", "OnRoll_ChatParser", strUnitName, nScore)
end

function DistributionView:OnBeginRoll_ChatParser(strUnitName, tItem)
	Utils:InvokeEventHandler(self, "DistributionView", "OnBeginRoll_ChatParser", strUnitName, tItem)
end

function DistributionView:OnEndRoll_ChatParser(strUnitName)
	Utils:InvokeEventHandler(self, "DistributionView", "OnEndRoll_ChatParser", strUnitName)
end

function DistributionView:OnRollHint_ChatParser(strUnitName, tHint)
	Utils:InvokeEventHandler(self, "DistributionView", "OnRollHint_ChatParser", strUnitName, tHint)
end

function DistributionView:OnParsingStarted_ChatParser()
	Utils:InvokeEventHandler(self, "DistributionView", "OnParsingStarted_ChatParser")
end

function DistributionView:OnParsingStoped_ChatParser()
	Utils:InvokeEventHandler(self, "DistributionView", "OnParsingStoped_ChatParser")
end

function DistributionView:OnPlayerInspectionStarted(tUnit, tRecipient)
	Utils:InvokeEventHandler(self, "DistributionView", "OnPlayerInspectionStarted", tUnit, tRecipient)
end

function DistributionView:OnPlayerInspectionStoped()
	Utils:InvokeEventHandler(self, "DistributionView", "OnPlayerInspectionStoped")
end

function DistributionView:OnOneSecondElapsed()
	self.nElapsedTime = self.nElapsedTime + 1
	self.wndElapsedTime:SetText(self.nElapsedTime)
end

----------------------------------------------------------------------------------
-- Forwarded events
----------------------------------------------------------------------------------
function DistributionView:OnFilterChange(wndHandler, wndControl, eMouseButton)
	self.tRecipientSet:OnFilterChange(wndHandler, wndControl, eMouseButton)
end

----------------------------------------------------------------------------------
-- Private functions
----------------------------------------------------------------------------------
function DistributionView:CallProtectedButton(strBtnName, strFunc, ...)
	if Utils:IsLeader() then
		local btn = self[strBtnName]
		
		if btn ~= nil then
			local fBtn = btn[strFunc]
			
			if fBtn ~= nil then
				fBtn(btn, ...)
			end
		end
	end
end

function DistributionView:UpdateRecipients()
	if self.tRecipientSet ~= nil then
		local tSelectedItem = self.tSelectedLootItem.tItem
	
		if tSelectedItem ~= nil then
			self.tRecipientSet:Update(tSelectedItem:GetEligibleRecipients(), true)
		else
			self.tRecipientSet:Clear()
		end
	end
end

function DistributionView:SendBeginRollToChat(bIsReroll, strTieString)
	local tLootItem  = self.tSelectedLootItem.tItem
	local nChNum     = Debug.bIsTest and ChatSystemLib.ChatChannel_Say or ChatSystemLib.ChatChannel_Party	
	local tChannel   = ChatSystemLib.GetChannels()[nChNum]
	local strPrefix  = "Roll for: "
	local strItem    = tLootItem:GetItem():GetChatLinkString()
	
	if bIsReroll then
		tChannel:Send(strTieString)	
		strPrefix = "Reroll for: "
	end
	
	-- Push the accepted format for a begin roll to the party channel.
	tChannel:Send(strPrefix .. strItem)
end

function DistributionView:SendEndRollToChat()
	local nChNum     = Debug.bIsTest and ChatSystemLib.ChatChannel_Say or ChatSystemLib.ChatChannel_Party	
	local tChannel   = ChatSystemLib.GetChannels()[nChNum]
	local strSummary = self.tRollSet:GetSummaryString()
	
	-- Push the accepted format for a end roll to the party channel.
	tChannel:Send(strSummary)
end

function DistributionView:UpdateVisibleButtonsHelper(b1, b2, b3, b4, b5)
	self:CallProtectedButton("btnAssign", "Show", b1)
	
	if self.btnBeginRoll ~= nil then
		self.btnBeginRoll:Show(b2)
	end
	
	if self.btnEndRoll ~= nil then
		self.btnEndRoll:Show(b3)
	end
	
	self:CallProtectedButton("btnRandomize", "Show", b4)
	
	if self.btnReroll ~= nil then
		self.btnReroll:Show(b5)
	end
end

function DistributionView:UpdateVisibleButtons()
	if self.eState == SelectionEnum.None then
		self:UpdateVisibleButtonsHelper(false, true, false, true, false)
	elseif self.eState == SelectionEnum.ItemOnly then
		self:UpdateVisibleButtonsHelper(false, true, false, true, false)
	elseif self.eState == SelectionEnum.ItemAndPlayer then
		self:UpdateVisibleButtonsHelper(true, true, false, false, false)
	elseif self.eState == SelectionEnum.RollingForItem then
		self:UpdateVisibleButtonsHelper(false, false, true, false, true)
	end
end

function DistributionView:Reset()
	self.tLootSet:UnselectAll()
	self.tRecipientSet:Clear()
	self.tSelectedRaider.tRecipient = nil
	self.tSelectedLootItem.tItem = nil
	self.tSelectedLootItem.wndInfo:Show(false)
	self.eState = SelectionEnum.None
	self:UpdateVisibleButtons()
	self:DisplayNoItemSelected()
end

function DistributionView:AssignLootHelper(bShouldRandomize)
	local tLootItem = self.tSelectedLootItem.tItem
	local tUnit     = nil 
	
	if bShouldRandomize then
		tUnit = tLootItem:GetRandomRecipient()
	else 
		local tRaider = self.tRecipientSet:GetSelectedRecipient()
		
		if tRaider ~= nil then
			tUnit = tRaider:GetUnit()
		end
	end
	
	if tUnit ~= nil and tLootItem ~= nil then
		RaidLooter:AssignMasterLoot(tLootItem, tUnit)
		self:Reset()
		self.tLootSet:RemoveItem(tLootItem)
		self.tLootSet:SelectFirst()
	end
end

function DistributionView:DisplayNoItemSelected()
	if self.tSelectedLootItem ~= nil then
		self.tSelectedLootItem.wndInfo:Show(false)
	end
	
	self:UpdateVisibleButtons()
	self.btnBeginRoll:Enable(false)
	self.btnRandomize:Enable(false)
end

function DistributionView:SyncToState(bIsRolling)
	local tLootItem = self.tSelectedLootItem.tItem

	if bIsRolling then
		if not tLootItem:IsSelected() then
			self:SelectSingleItem(tLootItem)
		end
	
		self.nElapsedTime = 0
		self.tElapsedTimer:Start()
		self.wndElapsedTime:SetText("0")
		self.tRollSet:Clear()
		self.tRollSet:BeginRollingForItem(tLootItem)
		self.tLootSet:Lock()
		self.btnReroll:Enable(false)
		self.wndItemStop:Show(true)
		self.wndItemLocked:Show(true)
		self.btnClearRolls:Show(false)
		
		self.eState = SelectionEnum.RollingForItem
	else
		if tLootItem ~= nil then
			tLootItem:SetIsRolledFor(true)
		end
		
		self.tElapsedTimer:Stop()
		self.tRollSet:EndRolling()
		self.tLootSet:Unlock()
		self.wndItemStop:Show(false)
		self.wndItemLocked:Show(false)
		self.btnClearRolls:Show(true)
		
		local tRoller, bIsTied = self.tRollSet:GetDistictTop()
	
		if Settings:ReadSetting("tLeader.bUseAutoSelection") then
			if tRoller ~= nil and bIsTied ~= nil and not bIsTied then
				local strUnitName = tRoller:GetName()
				local tRaider     = self.tRecipientSet:GetRaider(strUnitName)
				
				tRaider:Select() 
				
				-- We could not select the winner (maybe offline or out-of-range).
				-- So we disable the buttons for 3 seconds to avoid that the wrong persons ends 
				-- up with the item.
				if not tRaider:IsSelected() then
					self:CallProtectedButton("btnRandomize", "Enable", false)
					self:CallProtectedButton("btnAssign", "Enable", false)
					self.tResetTimer = ApolloTimer.Create(3, false, "EnableAssignAndRandomize", self)
				end
			end
		end
		
		if self.tSelectedRaider.tRecipient ~= nil then
			self.eState = SelectionEnum.ItemAndPlayer	
		else 
			self.eState = SelectionEnum.ItemOnly
		end	
	end
	
	self:UpdateVisibleButtons()
end

function DistributionView:EnableAssignAndRandomize()
	Utils:Log("EnableAssignAndRandomize")
	self:CallProtectedButton("btnRandomize", "Enable", true)
	self:CallProtectedButton("btnAssign", "Enable", true)
end

function DistributionView:SelectMultipleItems()
	self.tSelectedLootItem.bIsMultiple = true
	self.tSelectedLootItem.tItem       = nil
	self.tSelectedLootItem.wndName:SetText("Multiple items selected.")
	self.tSelectedLootItem.wndName:SetTextColor("ffffffff")
	self.tSelectedLootItem.wndIcon:SetSprite("") -- TODO
	
	if self.eState == SelectionEnum.ItemAndPlayer then
		self.tSelectedRaider.tRecipient = nil
	end
	
	self.eState = SelectionEnum.MultipleItems
	self.btnBeginRoll:Enable(false)
	self.tRecipientSet:Clear()
end

function DistributionView:SelectSingleItem(tSelectedItem)
	local strTextColor = Utils:GetItemQualityString(tSelectedItem)
	local wndItemName  = self.tSelectedLootItem.wndName
	local wndItemIcon  = self.tSelectedLootItem.wndIcon
	local wndItemInfo  = self.tSelectedLootItem.wndInfo
	
	self.tRecipientSet:Update(tSelectedItem:GetEligibleRecipients())
    self.tSelectedLootItem.tItem = tSelectedItem

	wndItemName:SetText(tSelectedItem:GetName())
	wndItemName:SetTextColor(strTextColor)
	wndItemIcon:SetSprite(tSelectedItem:GetIcon())
	
	if self.eState == SelectionEnum.None then
		wndItemInfo:Show(true)
		self.wndWarn:Show(false)
		self.btnBeginRoll:Enable(true)
		self:CallProtectedButton("btnRandomize", "Enable", true)
		
		local tItem = tSelectedItem:GetItem()

		if tItem ~= nil then
			Tooltip.GetItemTooltipForm(self, wndItemInfo, tItem, {
				bPrimary    = true, 
				bSelling    = false, 
				itemCompare = self:GetItemForComparision(tItem)
			})
		end
		
	elseif self.eState == SelectionEnum.ItemAndPlayer then
		self.tSelectedRaider.tRecipient = nil
	elseif self.eState == SelectionEnum.MultipleItems then
		self.tSelectedLootItem.bIsMultiple = false
		self.btnBeginRoll:Enable(true)
	end
	
	self.eState = SelectionEnum.ItemOnly
	self:UpdateVisibleButtons()
end

----------------------------------------------------------------------------------
-- Things not used (but still relevant)
----------------------------------------------------------------------------------
function DistributionView:HandleOnRequestAssistPremissions()
	-- Warning --
	-- This member is not used. 
	
	self.tAuth.nTickCount    = 15
	self.tAuth.tRequestTimer = ApolloTimer.Create(1, true, "OnRequestAssistPremissions_Tick", self)
	self.tAuth.btnRequest:Show(false)
	self.tAuth.wndRequest:SetText(self.tAuth.strRequestText .. " (" .. self.tAuth.nTickCount .. ") ...")
	self.tAuth.wndRequest:Show(true)
end

function DistributionView:HandleOnAssistPermissionsGranted()
	-- Warning --
	-- This member is not used. 
	
	-- Note: There should be or have been an event Group_MemberFlagsChanged.
	self.tAuth.wndRequest:Show(false)
	self.tAuth.tRequestTimer:Stop()
	
	if Debug.bIsTest then
		self:LoadFullViewFormAuthView(true)
		Debug.bIsAssistant = true
		self.bIsAuthorized = true
	end
end

function DistributionView:HandleOnAssistPermissionDenied()
	-- Warning --
	-- This member is not used. 
	
	self.tAuth.tRequestTimer:Stop()
	self.tAuth.wndRequest:Show(false)
	self.tAuth.wndDenied:Show(true)
	self.tAuth.tSpamTimer = ApolloTimer.Create(self.tAuth.nSpamTime, false, "OnRequestAssistPremissions_SpamProtection", self)
end

function DistributionView:HandleOnRequestAssistPremissions_Tick()
	-- Warning --
	-- This member is not used. 
	
	self.tAuth.nTickCount = self.tAuth.nTickCount - 1
	self.tAuth.wndRequest:SetText(self.tAuth.strRequestText .. " (" .. self.tAuth.nTickCount .. ") ...")
	 
	if self.tAuth.nTickCount <= 0 then
		self.tAuth.tRequestTimer:Stop()
		self.tAuth.wndTimeout:Show(true)
		self.tAuth.wndRequest:Show(false)
		
		self.tAuth.tTimeoutTimer = ApolloTimer.Create(self.tAuth.nTimeoutTime, false, "OnRequestAssistPremissions_Timeout", self)
	end
end

function DistributionView:HandleOnRequestAssistPremissions_Timeout()
	-- Warning --
	-- This member is not used. 
	
	self.tAuth.wndTimeout:Show(false)
	self.tAuth.btnRequest:Show(true)
	self.tAuth.tTimeoutTimer = nil
end

function DistributionView:HandleOnRequestAssistPremissions_SpamProtection()
	-- Warning --
	-- This member is not used. 
	
	self.tAuth.wndDenied:Show(false)
	self.tAuth.btnRequest:Show(true)
	self.tAuth.tSpamTimer = nil
end

ClassFactory:RegisterClass("DistributionView", DistributionView)
