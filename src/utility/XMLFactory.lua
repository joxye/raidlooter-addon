--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
-- XMLFactory 
-- This is a convenience wrapper object that handle loading and unloading 
-- of XML data. We set it up so all XMLs are loaded/unloaded when the window 
-- toggles. Some XMLs are required multiple times and as such they aren't unloaded
-- until the player leaves the raid.
--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
function XMLFactory:LoadAll(strEventHandler, tObject)
	Utils:Log("All XML files will be loaded.")

	self.tXMLs = {}
	self.tCallback = {
		strName = strEventHandler,
		tObject = tObject
	}
	self.nLoaded = 0
	self:RegisterXML("Base",                "RaidLooter.xml",          "Container",    false)
	self:RegisterXML("Roller",              "Roller.xml",              "Container",    true)
	self:RegisterXML("Raider",              "Raider.xml",              "RaiderBase",   true)
	self:RegisterXML("LootItem",            "LootItem.xml",            "LootItemBase", true)
	self:RegisterXML("ItemDialog",          "AlertDialog.xml",         "Dialog",       true)
	self:RegisterXML("Overlay",             "Overlay.xml",             "Overlay",      false)
	self:RegisterXML("LootView",            "LootView.xml",            "Container",    false)
end

function XMLFactory:RegisterXML(strAlias, strFilename, strXmlRootName, bIsMultiUse)
	local tXmlDoc = XmlDoc.CreateFromFile(strFilename)
	tXmlDoc:RegisterCallback("OnXMLLoaded", self)

	self.tXMLs[strAlias] = { 
		strFilename    = strFilename, 
		strXmlRootName = strXmlRootName,
	    bIsLoaded      = false,
		tXmlDoc        = tXmlDoc,
		bIsMultiUse    = bIsMultiUse
	}
end

function XMLFactory:Unload(strAlias)
	Utils:Log("XML file with alias " .. strAlias .. " unloaded.")
	self.tXMLs[strAlias] = nil
end

function XMLFactory:UnloadAll()
	Utils:Log("All XML files will be unloaded.")
	self.tXMLs = {}
	self.tCallback = nil
	self.nLoaded = 0
end

function XMLFactory:CreateWindow(strAlias, wndParent, tEventHandler)
	local tInfo = self.tXMLs[strAlias]
	
	if tInfo ~= nil then
		if tInfo.tXmlDoc:IsLoaded() then
			local wnd = Apollo.LoadForm(tInfo.tXmlDoc, tInfo.strXmlRootName, wndParent, tEventHandler) 
			
			if wnd == nil then
				Utils:LogError("XMLFactory:CreateWindow", "Creating the window failed - make sure all files are up-to-date.")
				Utils:LogBacktrace()
			end
			
			if not tInfo.bIsMultiUse then
				self:Unload(strAlias)
			end
			
			return wnd
		else
			Utils:LogError("XMLFactory:CreateWindow", "Unable to create the window because the XML is not loaded.")
			Utils:LogBacktrace()
		end
	else
		Utils:LogError("XMLFactory:CreateWindow", "Unable to find info for alias {" .. strAlias .. "}.")
		Utils:LogBacktrace()
	end

	return nil
end

function XMLFactory:CreateTopLevelWindow(strAlias, tEventHandler)
	return self:CreateWindow(strAlias, nil, tEventHandler)
end

function XMLFactory:OnXMLLoaded()
	local nTotal = Utils:TableLength(self.tXMLs)
	self.nLoaded = self.nLoaded + 1
	Utils:Log("XML-file loaded (" .. self.nLoaded .. "/" .. nTotal .. ")")
	
	if self.nLoaded == nTotal then
		if self.tCallback ~= nil then
			local tObject = self.tCallback.tObject
			local strFunc = self.tCallback.strName
			
			tObject[strFunc](tObject)
			self.tCallback = nil
		end
	end
end

-- Load
XMLFactory = RaidLooter:LoadSingleton("XMLFactory", XMLFactory)
