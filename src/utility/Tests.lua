--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
-- Tests   
-- Defines varaibles and functions used while developing.
--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--

----------------------------------------------------------------------------------
-- Initialization
----------------------------------------------------------------------------------
function Debug:New()
	local meta = {
		bIsTest          = false,
		bIsTracingEvents = false,
		bIsDebug         = false,
		bAllowLink       = true,
		bIsLeader        = true,
		bIsAssistant     = false,
		bIsInRaid        = true,
		tMasterLoot      = {},
		tRecipients      = {}
	}
	
	return ClassFactory:SetupInstance(meta, Debug)
end

function Debug:OnLoad()
	if self.bIsTest then
		Apollo.RegisterSlashCommand("rl-test", "OnRaidLooterTestCmd", self)
	end
end

function Debug:GenerateTestLoot(nX)
	local tPlayerUnit    = GameLib.GetPlayerUnit()
	local tInventoryLoot = tPlayerUnit:GetInventoryItems()
	local nItems         = Utils:TableLength(tInventoryLoot)
	
	for i=1,nX do 
		local nIndex = Utils:Random(nItems)
		local tItem = tInventoryLoot[nIndex]
		
		Utils:Log(tItem)
		
		table.insert(self.tData.tMasterLoot, {
			bIsMaster = true,
			itemDrop  = tItem.itemInBag,
			nLootId   = Utils:Random(1000000),
			tLooters  = {
				[1] = tPlayerUnit
			},
			tLootersOutOfRange = {
			},
		})
	end
end

----------------------------------------------------------------------------------
-- Functions
----------------------------------------------------------------------------------
function Debug:OnRaidLooterTestCmd(cmd, params)
	if not self.bIsTest then 
		return 
	end

	local cmd, value, value2 = Utils:Split(params, " ")
	
	if cmd == "show" then
		self:ShowAvailbleCommands()
	elseif cmd == "genLoot" then
		Utils:GenerateTestLoot(tonumber(value))
		RaidLooter:OnMasterLootUpdate()
	elseif cmd == "rand" then
		Utils:PrintRandomNumbers(tonumber(value))
	else
		self:GenerateTestLoot(10)
		ChatSystemLib.Command("/rl")
		ChatSystemLib.Command("/rover")
	end
end

function Debug:ShowAvailbleCommands()
	Utils:Log("\\rl-test show - Shows available test profiles.")
	Utils:Log("\\rl-test genLoot X - Generate X items using items from the player's inventory")
	Utils:Log("\\rl-test - /rl, /rover and generates some test loot.")
end

RaidLooter:RegisterSingleton("Debug", Debug)
