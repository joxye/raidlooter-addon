--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
-- Utils
-- Contains general utilities such as logging and copy operations. It also 
-- contain some convenience functions and event operations. 
--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--

----------------------------------------------------------------------------------
-- Debug logging
----------------------------------------------------------------------------------
function Utils:Log(var)
	if Debug.bIsDebug then
		Print(tostring(var))
		
		-- If the developer tool "rover" is installed the variable is forwaded 
		-- there as well as printed to the chat.
		if SendVarToRover then
			SendVarToRover("RaidLooter", var, 0)
		end
	end
end

function Utils:LogInfo(var)
	if Debug.bIsDebug then
		Print(tostring(var))
	end
end

function Utils:LogBacktrace(strErr)
	if Debug.bIsDebug then
		if strErr ~= nil then
			Print(debug.traceback(strErr))
		else
			Print(debug.traceback())
		end
	end
end

function Utils:LogError(strWhere, var)
	if var ~= nil then
		Apollo.AddAddonErrorText(strWhere, "Error: " .. var)
	else 
		Apollo.AddAddonErrorText(strWhere, "Unknown error")
	end
	
	Utils:LogInfo(var)
end

----------------------------------------------------------------------------------
-- Copy operations
----------------------------------------------------------------------------------
function Utils:Shallowcopy(orig)
	if orig == nil then return nil end

    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in pairs(orig) do
            copy[orig_key] = orig_value
        end
    else -- plain types.
        copy = orig
    end
    return copy
end

function Utils:Deepcopy(orig)
	if orig == nil then return nil end

    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[Utils:Deepcopy(orig_key)] = Utils:Deepcopy(orig_value)
        end
        setmetatable(copy, Utils:Deepcopy(getmetatable(orig)))
    else -- plain types.
        copy = orig
    end
    return copy
end

----------------------------------------------------------------------------------
-- Events
----------------------------------------------------------------------------------
function Utils:InvokeEventHandler(tInstance, strInstanceName, strEventHandler, ...)
	-- This will incur a small overhead on invoking an event 
	-- But it will add debug tracing to all event handlers so we can see if, when 
	-- and the order they are invoked.
	--
	-- WON'T FIX: 
	---  This **should** be changed. Prefixing with "Handle" is not
	--   good as it hides the real function name. What is passed in is
	--   what we should call.
	local strFuncName      = 'Handle' .. strEventHandler
	local strQualifiedName = strInstanceName .. ':' .. strFuncName
	
	if Debug.bIsTracingEvents then
		Utils:Log(strQualifiedName .. ' {triggered}')
	end
	
	self:FireEvent(tInstance, strFuncName, ...)
end

function Utils:FireEvent(tTarget, strEventName, ...)
	if tTarget ~= nil and strEventName ~= nil then
		local fTarget = tTarget[strEventName]

		if fTarget ~= nil then
			return fTarget(tTarget, ...)
		else 
			Utils:LogError('Utils:FireEvent', 'Target is not responding to :' .. strEventName)
		end
	end
end

----------------------------------------------------------------------------------
-- Other
----------------------------------------------------------------------------------
function Utils:GetMemberInfoMatchingPredicate(fPredacte)
	local nMembers = GroupLib.GetMemberCount()
	for nIndex = 1, nMembers do
		local tUnitMember = GroupLib.GetUnitForGroupMember(nIndex)
		
		-- WARNING --
		--     1. This will be nil if the player is out-of-range.
		--     2. This can be non-nil if the player is on the edge i.e.
		--        about to be destroyed so we must check the validity of 
		--        the unit before proceeding. 
		if tUnitMember ~= nil and tUnitMember:IsValid() then
			if fPredacte(tUnitMember) then
				local tMember = GroupLib.GetGroupMember(nIndex)
				return  {
					tUnit   = tUnitMember,
					tMember = tMember,
					nIndex  = nIndex
				}
			end
		end
	end
	
	return nil
end

function Utils:MoveWindow(wnd, tLoc)
	if wnd == nil or tLoc == nil then
		return
	end

	local tWndLoc = WindowLocation.new(tLoc)
	local nLeft, nTop, nRight, nBottom = tWndLoc:GetOffsets()
	local nWidth = nRight - nLeft
	local nHeight = nBottom - nTop
	
	-- Only move the window if the width and height are the same
	if nWidth == wnd:GetWidth() and nHeight == wnd:GetHeight() then
		wnd:MoveToLocation(tWndLoc)
	end
end
		
function Utils:IsAssistant()
	if Debug.bIsTest then
		return Debug.bIsAssistant
	end

	local tMemberInfo = self:GetMemberInfoMatchingPredicate(function(tUnitMember)
		return tUnitMember:IsThePlayer()
	end)
	
	if tMemberInfo ~= nil and tMemberInfo.tMember ~= nil then
		return tMemberInfo.tMember.bRaidAssistant
	end

	return false
end

function Utils:IsLeader() 
	return GroupLib.AmILeader() or Debug.bIsLeader
end

function Utils:IsInRaid()
	return GroupLib.InRaid() or Debug.bIsInRaid
end

function Utils:GetPlayerName()
	local tPlayerUnit = GameLib.GetPlayerUnit()
	
	if tPlayerUnit ~= nil then 
		return tPlayerUnit:GetName()
	end
	
	return ""
end

function Utils:IsMasterLootingOn()
	if Debug.bIsTest then
		return true
	end

	if Utils:IsInRaid() then
		local tLootRules = GroupLib.GetLootRules()
		return tLootRules ~= nil and (tLootRules.eNormalRule == GroupLib.LootRule.Master or tLootRules.eThresholdRule == GroupLib.LootRule.Master)
	end
	
	return false
end

function Utils:HaveArmorType(nClassId, strType)
	return strType == "A" or ClassInfo[nClassId].strArmorType == strType
end

function Utils:IsTableEmpty(t)
	return next(t) == nil
end

function Utils:GetItemQualityString(tItem)
	if tItem ~= nil then
		return ItemInfo[tItem:GetQuality()].strName 
	end
	return ""
end

function Utils:GetItemRarity(tItem)
	if tItem ~= nil then
		return ItemInfo[tItem:GetQuality()].strRarityBorder
	end
	return ""
end

function Utils:GetItemExtendedRarity(tItem)
	if tItem ~= nil then
		return ItemInfo[tItem:GetQuality()].strExtendedRarityBorder
	end
	return ""
end

function Utils:GetClassIcon(tUnit)
	if tUnit ~= nil and tUnit:IsValid() then
		return ClassInfo[tUnit:GetClassId()].strIcon
	end
	return ""
end

function Utils:TableLength(tTable)
	local nCount = 0
	for _ in pairs(tTable) do nCount = nCount + 1 end
	return nCount
end

-- Compatibility: Lua-5.1
function Utils:Split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
	 table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return unpack(t)
end

-- Source: http://lua-users.org/wiki/MakingLuaLikePhp
-- Credit: http://richard.warburton.it/
function Utils:Explode(div, str)
    if (div=='') then return false end
    local pos,arr = 0,{}
    for st,sp in function() return string.find(str,div,pos,true) end do
        table.insert(arr,string.sub(str,pos,st-1))
        pos = sp + 1
    end
    table.insert(arr,string.sub(str,pos))
    return arr
end

function Utils:PrintRandomNumbers(nN)
	tResult = ""
	for nIndex=1,nN do
		tResult = tResult .. Utils:Random(100)
		
		if nIndex ~= nN then
			tResult = tResult .. ", "
		end
	end
	
	self:Log("Random number: " .. tResult)
end

-- Improved version of the built-in pseudorandom generator
--     See: http://lua-users.org/wiki/MathLibraryTutorial
--
-- Wrapped up in a custom function so the global definition isn't changed. 
function Utils:Random(S)
	if Utils.bInternal_IsRandomNumbersInitilized == nil then
		Utils.bInternal_IsRandomNumbersInitilized = true
		
		-- Set a seed and discard first 3.
		math.randomseed(os.time())
		math.random(); math.random(); math.random()
	end
	
	-- This will indeed call math.random 97+1 times 
	-- but the result appears a lot more "random".
	if randomtable == nil then
		randomtable = {}
		for i = 1, 97 do
			randomtable[i] = math.random()
		end
	end
	
	-- x acts as an index weight. 
	local x = math.random()
	local i = 1 + math.floor(97*x)
	
	-- Perform a swap, x will become the actual random number 
	-- at index i.
	x, randomtable[i] = randomtable[i], x

	-- Adjust the range if S is provided.
	if S ~= nil then
		local r = (x * (S - 1))
		local result = math.floor(r + 1.5) -- [1, S]
		
		Utils:Log("Random =>" .. result)
		return result
	end 
	
	return x -- [0,1]
end

function Utils:PlaySound(nSoundFx)
	local bUseSound = Settings:ReadSetting("tGeneral.bUseSound")
	
	if bUseSound then
		Sound.Play(nSoundFx)
	end
end

function Utils:PlayNewLootSound()
	local bUseLootSound = Settings:ReadSetting("tGeneral.bUseLootSound")
	
	if bUseLootSound then
		self:PlaySound(197)
	end
end

function Utils:PlayNewRollSound()
	local bUseRollSound = Settings:ReadSetting("tGeneral.bUseRollSound")
	
	if bUseRollSound then
		self:PlaySound(231)
	end
end

-- Load
Utils = RaidLooter:LoadSingleton("Utils", Utils)
