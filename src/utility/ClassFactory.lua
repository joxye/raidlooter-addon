--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
-- ClassFactory
-- The class factory is simple concept for registering class-prototypes and 
-- at a later time create/clone an instance. 
--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
function InitPrototype(tPrototype)
	tPrototype.__index = tPrototype
	return tPrototype
end

function ClassFactory:SetupInstance(tClass, tPrototype)
	local tInstance = setmetatable(tClass, tPrototype)
	self:LinkInstance(RaidLooter, tInstance)
	return tInstance
end

function ClassFactory:LinkInstance(tTarget, tSource)
	if self.bAllowLink then
		-- WARNING --
		-- This causes faults/exceptions thrown by a non-main module to propagate correctly, 
		-- that is, it allows you to see the stack. If you skip this line the "module" would 
		-- simply stop without notify you (or anyone else). A module in this case is a object
		-- you called setmetatable on. 
        -- 
		-- Unfortently this line also causes the memory calculation to go hell. Simply opening 
		-- and poking around for a few minutes report 100 Mb+ of memory usage. I guess it's a 
		-- false-positive?! as the addon works just fine (no lag or anything) but opening the 
		-- Addon-list (ESC) will cause Wildstar to hang/crash (eventually). 
		-- 
		-- I therefore only use this when developing!
		Apollo.LinkAddon(tTarget, tSource)
	end
end

function ClassFactory:RegisterClass(strName, tClassPrototype)
	ClassFactory.tPrototypes[strName] = InitPrototype(tClassPrototype)
end

function ClassFactory:GetClassPrototype(strName)
	local tClassPrototype = ClassFactory.tPrototypes[strName]
	if tClassPrototype == nil then
		Utils:LogError(self, "No such ClassPrototype {" .. strName .. "}. Make sure you have all files.")
		Utils:LogBacktrace()
		return nil
	end 
	return tClassPrototype
end

function ClassFactory:CreateInstance(strName, ...)
	local tClassPrototype = self:GetClassPrototype(strName)
	if tClassPrototype == nil then
		return nil
	end
	
	local fConstructor = tClassPrototype["New"]
	
	if fConstructor ~= nil then
		return fConstructor(tClassPrototype, ...)
	end
	
	return tClassPrototype
end

ClassFactory.tPrototypes = {}
ClassFactory.bAllowLink = false
ClassFactory = RaidLooter:LoadSingleton("ClassFactory", ClassFactory)
