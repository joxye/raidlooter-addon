--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--
-- Tests   
-- Defines varaibles and functions used while developing.
--::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::--

----------------------------------------------------------------------------------
-- Initialization
----------------------------------------------------------------------------------
function Debug:New()
	local bIsDeveloping = false
	local meta = {
		bIsTest          = bIsDeveloping,
		bIsTracingEvents = bIsDeveloping,
		bIsDebug         = bIsDeveloping,
		bIsLeader        = false,
		bIsAssistant     = false,
		bIsInRaid        = false,
		tMasterLoot      = {},
		tRecipients      = {}
	}
	
	return ClassFactory:SetupInstance(meta, Debug)
end

----------------------------------------------------------------------------------
-- Events
----------------------------------------------------------------------------------
function Debug:OnLoad()
	if self.bIsTest then
		Apollo.RegisterSlashCommand("rl-test", "OnRaidLooterTestCmd", self)
	end
end

function Debug:OnRaidLooterTestCmd(cmd, params)
	if not self.bIsTest then 
		return 
	end

	local cmd, value, value2 = Utils:Split(params, " ")
	
	if cmd == "genLoot" then
		self:GenerateTestLoot(tonumber(value))
		RaidLooter:OnMasterLootUpdate()
	elseif cmd == "rand" then
		Utils:PrintRandomNumbers(tonumber(value))
	elseif cmd == "settings" then
		self:PrintSettings(value)
	elseif cmd == "write-setting" then
		self:WriteSetting(value, value2)
	elseif cmd == "read-setting" then
		self:PrintSetting(value)
	else
		self:GenerateTestLoot(15)
		ChatSystemLib.Command("/rl")
		ChatSystemLib.Command("/rover")
	end
end

----------------------------------------------------------------------------------
-- Functions
----------------------------------------------------------------------------------
function Debug:GenerateTestLoot(nX)
	local tPlayerUnit    = GameLib.GetPlayerUnit()
	local tInventoryLoot = tPlayerUnit:GetInventoryItems()
	local nItems         = Utils:TableLength(tInventoryLoot)
	
	for i=1,nX do 
		local nIndex = Utils:Random(nItems)
		local tItem = tInventoryLoot[nIndex]
		
		table.insert(self.tMasterLoot, {
			bIsMaster = true,
			itemDrop  = tItem.itemInBag,
			nLootId   = Utils:Random(1000000),
			tLooters  = {
				[1] = tPlayerUnit
			},
			tLootersOutOfRange = {
			},
		})
	end
end

function Debug:WriteSetting(strKeySequnce, value)
	Settings:WriteSetting(strKeySequnce, value)
end

function Debug:PrintSetting(strKeySequnce)
	Utils:Log(Settings:ReadSetting(strKeySequnce))
end

function Debug:PrintSettings()
	if value == "default" then
		Utils:Log("Default Settings")
		self:PrintSettingsHelper(Settings:GetDefaultTable(), "")
	elseif value == "new" then
		Utils:Log("Saved Settings")
		self:PrintSettingsHelper(Settings:GetNewTable(), "")
	else
		Utils:Log("Current Settings")
		self:PrintSettingsHelper(Settings:GetTable(), "")
	end
end

function Debug:PrintSettingsHelper(tSettings, indent)
	for strKey, value in pairs(tSettings) do
		if type(value) == 'table' then
			Utils:Log(indent..strKey .. " => {")
			self:PrintSettingsHelper(value, indent.."  ")
			Utils:Log(indent.."}")
		else 
			Utils:Log(indent..strKey .. " => " .. tostring(value))
		end
	end
end

-- Load
Debug = RaidLooter:LoadSingleton("Debug", Debug)
