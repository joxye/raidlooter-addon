Welcome to Raid Looter
================
This is an open repository. 
____

This is an addon for Wildstar. It intend to make loot distribution during raids easier by providing
a set of simple tools to the master looter. 

This addon can be used by anyone but the ONLY one that really needs 
it is the raider leader. This addon will show a compact overview of item's 
ready for distribution. The leader or raid assistant can initiate a roll for 
an item. The item will be linked to the party chat and all rolls made with 
/roll durring the roll-window will show in the addon window. 

The raid leader can assign items and/or randomize items. The leader may 
select multiple items at once for randomization. Eventhough raid assistants 
can access the management window, it is ONLY the raid leader that can 
distribute the loot.

Raiders will only see the LootWindow at which they can view the loot and see 
the latest rolls.